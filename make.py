# This is a script to make Atlas.
# I hate Makefiles so hopefully
# this will do some good.

import os

compiler = "g++"
cflags = "-std=c++11 -Wall -Werror"
exe  = "atlas"

log = "ATLAS_LOG_ON"
dbg = "ATLAS_DEBUG_ON"

src_dir = "./src/"
build_dir = "./build/"
inc_dir = "./include/"
bin_dir = "./bin/"
sub_dirs = ["lex", "parse"]

libs = ""

def get_src(path):
    return get_files(path, ".cpp")

def get_headers(path):
    return get_files(path, ".hpp")

def get_objects(path):
    return get_files(path, ".o")

def get_files(path, ext):
    files = os.listdir(path)
    files = map(lambda f: path + f, files)
    return filter(lambda f: f.endswith(ext), files)

def get_inc():
    inc = "-I" + inc_dir + " "
    for dirr in sub_dirs:
        inc = inc + "-I" + inc_dir + dirr + " "
    return inc

def create_objs(srcs):
    return map(lambda f: f.replace(".cpp", ".o"), srcs)

def concat(file_names):
    out = ""
    for f in file_names:
        out = out + f + " "
    return out

def move_files(src, dest, ext):
    files = os.listdir(src)
    files = filter(lambda f: f.endswith(ext), files)
    for file in files:
        os.rename(src + file, dest + file)

def make_cmd(cmd, opts):
    for k, v in opts.iteritems():
        cmd = cmd.replace(k, v)
    return cmd

def make_objects():
    cmd = "(CC) (CFLAGS) (INC) -c (SRCS) -D(LOG) -D(DBG)"
    all_src = get_src(src_dir)
    for dirr in sub_dirs:
        all_src.extend(get_src(src_dir + dirr + "/"))
    srcs = concat(all_src)
    opts = {
        "(CC)": compiler,
        "(CFLAGS)": cflags,
        "(INC)": get_inc(),
        "(SRCS)": srcs,
        "(LOG)": log,
        "(DBG)": dbg
    }
    cmd = make_cmd(cmd, opts)
    print "Compiling source: \n" + cmd
    res = os.system(cmd)
    return os.WEXITSTATUS(res)

def make_exe():
    cmd = "(CC) (CFLAGS) (INC) -o (EXE) (OBJS) (LIBS) -D(LOG) -D(DBG)"
    obj_files = get_objects(build_dir)
    objs = concat(obj_files)
    opts = {
        "(CC)": compiler,
        "(CFLAGS)": cflags,
        "(INC)": get_inc(),
        "(EXE)": bin_dir + exe,
        "(OBJS)": objs,
        "(LIBS)": libs,
        "(LOG)": log,
        "(DBG)": dbg
    }
    cmd = make_cmd(cmd, opts)
    print "Compiling exe: \n" + cmd
    res = os.system(cmd)
    return os.WEXITSTATUS(res)

def clean():
    print "Cleaning build dir...\n"
    for f in os.listdir(build_dir):
        os.remove(build_dir + f)

def make():
    res = make_objects()
    if res == 0:
        print "Compiled source successfully!!!\n"
        move_files("./", build_dir, ".o")
        res = make_exe()
        if res != 0:
            print "\nError compiling {0} - exit code: {1}".format(exe, res)
        else:
            print "\nCompiled {0} successfully!!!\n".format(exe)
        clean()
        print "Done!\n"
    else:
        print "Error compiling source - exit code: {0}".format(res)

if __name__ == '__main__':
    make()
