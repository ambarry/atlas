/* test.cpp
 * Runs all the tests
 */


#include "../src/logger.hpp"
#include "lexer_test.hpp"

namespace test {

  using logger::log;

  int main(int argc, char const *argv[]) {
    log("test runner", "starting test suite...");
    run_lex_tests();

  }
}
