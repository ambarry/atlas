# Newlines in .\TLAS



## TODO: rework!

- going to use an F# style offside rule
- first non-whitespace char following =, or -> starts the next col
 + BUT ONLY IF A NEWLINE HAPPENED
- push onto stack (insert indent or block_start (rework grammar!)
- if col becomes less, pop
  + check w/ prev, keep popping like dedents, check for error
  + for each pop, insert a block end
  + final may need an e sort of char (may need anyway)
- you just have to check cols at line starts
- let comments through
- may still want newline as a marker? is this needed? what happens if indent and no block start, e.g. arg list or pipe? do we care?
  + if we care, keep newlines as tokens, use the ^ op to extend func calls
  + if we don't care, just keep going until they pop, discarding all indents and newlines that don't matter!!!




## Block Expressions

- newline + indent indicates a block expr and pops the newline
- certain forms expect blocks, e.g. match, loop
- others allow (=, ->)
- these are all that really need to
- NOTE: do we actually need blocks if we use the offside scheme?

## Piping

- forward pipe should be directly under the offside marker

## Line continuation

- line continuation should be directly under the offside marker
- not really line continuation so much as argument continuation
  + may need some actual parser rules for this, rather than lexer manipulation, in the arg list
    - e.g. newline ^ arglist (but no indents or dedents!!!)

## Containers

- containing units such as [, {, ( will ignore newlines until closed
- the following should be equivalent:
- ignore idents too and remove from the parser

```
x:[int:3] = [0,1,2]

# and

x:[int:3] = [0,
             1,
             2]

# and

x:[int:3] = [0,
            1,2]

```

## Line continuation

- for function calls (or pipe operator, treat the same!) we need something
- come up with how you want it to look
- come up with how to make it work
- make it work
- (sep piping if needed...)
- you are NOT tied to what exists
- could say, if you chose not to indent, mark start of post = sign col, ident after that?

```

x: int = do_stuff first_param
         ^ second_param
         ^ third_param
	 ^ fourth
         |> something else


y: int = do_stuff first_param
         |> another_func
         |> another_func2


y: int =
  do_stuff first_param
  |> another_func
  |> another_func2


z: int = match x:
         | something -> 2
         |> pipe_must_be_on_level?                   


a: int = match x:
         | case1 -> single_line
         | case2 ->
           takes
           |> two
         | case3 ->
           b: int = 3
           c: string = "this is another multi-statement block"
           b
         | case4 -> func long_arg
                    ^ line_contin_arg1
                    ^ line_contin_arg2
         | case5 -> a
                    |> b
                    |> c
         |> acts_on_match_res 7


# even cleaner
a: int =
  match x with
    | case1 -> single_line
    | case2 ->
      takes
      |> two
    | case3 ->
      b: int = 3
      c: string = "this is another multi-statement block"
      b
    | case4 -> func long_arg
               ^ line_contin_arg1 # could be placed anywhere? or strict...
               ^ line_contin_arg2
    | case5 ->
      a
      |> b # look bad but legal, so preferable to...
      |> c
         ^ 5 # precedence?
  |> acts_on_match_res 7 # issue (we required indent on others, not here?



x: int =
  y: int =
    do_stuff first_param
    ^ second_param
    ^ third_param
