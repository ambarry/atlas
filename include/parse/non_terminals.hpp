#ifndef NON_TERMINALS_H
#define NON_TERMINALS_H

#include <stack>
#include <string>
#include "production.hpp"
#include "terminals.hpp"
#include "token.hpp"

#define NT(classname) \
class classname : public NonTerminal { \
public: \
  ~classname() override {} \
  bool expand(Token &tok, Prods &prods) override; \
  string info() override {return #classname;} \
};

namespace parse {

  using lex::Token;
  using std::stack;
  using std::string;

  // -----------------------------------------------
  // --- Helpers ---
  // -----------------------------------------------

  bool is_arithmetic_op_token(Token& tok);
  bool is_type_token(Token& tok);
  bool is_expr_token(Token& tok);
  bool is_term_token(Token& tok);
  bool is_relop_token(Token& tok);
  bool is_factor_token(Token& tok);

  // -----------------------------------------------
  // --- Non-terminals ---
  // -----------------------------------------------

  NT(Goal)

  // -----------------------------------------------
  // --- Definition ---
  // -----------------------------------------------

  NT(Definition)
  NT(Index)
  NT(Id_List)
  NT(Id_List_Prime)
  NT(Struct_Definition)
  NT(Field_Def_List)
  NT(Field_Def_List_Prime)
  NT(Union_Definition)
  NT(Union_Case_List)
  NT(Union_Case_List_Prime)
  NT(Opt_Union_Type)

  // -----------------------------------------------
  // --- Types ---
  // -----------------------------------------------

  NT(Type)
  NT(Type_List)
  NT(Type_List_Prime)
  NT(Type_Postfix)
  NT(Type_Prime)
  NT(Standard_Type)
  NT(Built_In_Type)
  NT(Generic_Suffix)
  NT(Opt_Generic_Suffix)
  NT(Tuple_Type_List)
  NT(Tuple_Type_List_Prime)
  NT(List_Type)
  NT(Array_Type)

  // -----------------------------------------------
  // --- Primary Expressions ---
  // -----------------------------------------------

  NT(B_Expr)
  NT(Expr_List)
  NT(Expr_List_Prime)
  NT(Expr)
  NT(Expr_Prime)
  NT(Forward_Pipe_Expr)
  NT(Term_Expr)
  NT(Term_Postfix_Expr)
  NT(Term_Postfix)
  NT(Argument_List)
  NT(Argument_List_Prime)
  NT(Term_Prefix_Expr)
  NT(Term)
  NT(Term_Prime)
  NT(Dollar_Term)
  NT(Dot_Expr)
  NT(Dot_Op_Postfix)
  NT(Factor)
  NT(B_Term_Expr_List)
  NT(Term_Expr_List)
  NT(Term_Expr_List_Prime)
  NT(B_Assignment_List)
  NT(Assignment_List)
  NT(Assignment_List_Prime)

  // -----------------------------------------------
  // --- Lambda Expressions ---
  // -----------------------------------------------

  NT(Lambda_Expr)
  NT(Binding_List)
  NT(Binding_List_Prime)

  // -----------------------------------------------
  // --- Pointer Expressions ---
  // -----------------------------------------------

  NT(Ptr_Expr)

  // -----------------------------------------------
  // --- Boolean Expressions ---
  // -----------------------------------------------

  NT(Bool)
  NT(Bool_Postfix)
  NT(Rel_Postfix)
  NT(Rel)
  NT(Bool_Prefix_Expr)

  // -----------------------------------------------
  // --- Arithmetic Expressions ---
  // -----------------------------------------------

  NT(Arithmetic_Postfix)
  NT(Arithmetic_Op)

  // -----------------------------------------------
  // --- Pattern Matching Expressions ---
  // -----------------------------------------------

  NT(Match_Expr)
  NT(Rule_List)
  NT(Rule_List_Prime)
  NT(Rule)
  NT(Pattern)
  NT(Pattern_Guard)
  NT(Pattern_Factor_Postfix)
  NT(Collection_Pattern)
  NT(Tuple_Pattern)
  NT(Pattern_List)
  NT(Pattern_List_Prime)

}

#endif
