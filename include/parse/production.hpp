#ifndef PRODUCTION_H
#define PRODUCTION_H

#include <stack>
#include <string>
#include "token.hpp"

namespace parse {

  class Production {
  public:
    virtual ~Production(){}
    virtual bool is_terminal() = 0;
    virtual bool is_match(lex::Token& tok) = 0;
    virtual bool expand(lex::Token &tok, std::stack<Production*> &prods) = 0;
    virtual bool is_eof() = 0;
    virtual std::string info() = 0;
  };

  typedef std::stack<Production*> Prods;

  class Terminal : public Production {
  public:
    virtual ~Terminal() override {}
    virtual bool is_terminal() override {return true;}
    virtual bool is_match(lex::Token& tok) override {return false;}
    virtual bool is_eof() {return false;}
    virtual bool expand(lex::Token &tok, Prods &prods) override {
      return false;}
    virtual std::string info() override {return "Terminal";}
  };

  class NonTerminal : public Production {
  public:
    virtual ~NonTerminal() override {}
    virtual bool is_terminal() override {return false;}
    virtual bool is_match(lex::Token& tok) override {return false;}
    virtual bool is_eof() {return false;}
    virtual bool expand(lex::Token &tok, Prods &prods) override {return false;}
    virtual std::string info() override {return "NonTerminal";}
  };

  class EndSentinal : public NonTerminal {
    ~EndSentinal() override {}
    virtual bool is_eof() override {return true;}
    virtual bool expand(lex::Token &tok, Prods &prods) override {
      return false;
    }
    virtual std::string info() override {return "EndSentinal";}
  };
}

#endif
