#ifndef PARSER_H
#define PARSER_H

#include <stack>
#include <string>
#include <sstream>
#include <vector>

#include "logger.hpp"
#include "token.hpp"

namespace parse {

  using lex::Token;
  using std::vector;
  using std::stack;

  class Parser {
  public:
    Parser(){};
    int parse(vector<Token*>& tokens, SymbolTable& sym, TypeTable& tt); // TODO: stream
  };

  // --- log helpers ---

  inline void log(std::string msg) {
    logger::log("parser", msg);
  }

  inline void debug(std::string msg) {
    logger::debug("parser", msg);
  }

  inline void debug(std::stringstream& msg){
    logger::debug("parser", msg);
  }

  inline void error(std::string prefix, std::string msg, int line) {
    std::stringstream s;
    s << prefix << " ERROR: Line " << line << std::endl;
    s << "           >> " << msg << std::endl;
    logger::error("parser", s);
  }

  inline void terminal_error(std::string prefix, std::string msg,
    std::string terminal, std::string focus, int line) {
      std::stringstream s;
      s << msg << std::endl;
      s << "   Expected symbol: " << focus << std::endl;
      s << "   Found: " << terminal << std::endl;
      error(prefix, s.str(), line);
  }

  inline void non_terminal_error(std::string prefix, std::string msg,
    std::string token, std::string focus, int line) {
      std::stringstream s;
      s << msg << std::endl;
      s << "   Current production: " << focus << std::endl;
      s << "   Current token: " << token << std::endl;
      error(prefix, s.str(), line);
  }
}


#endif
