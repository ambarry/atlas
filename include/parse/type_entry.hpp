#ifndef TYPE_ENTRY_H
#define TYPE_ENTRY_H

#include <string>

namespace parse {

  using std::string;

  /// base class for type info record
  /// details will change depending on struct, union, function, array, list, etc.
  /// going to have to revisit w/ generics...
  /// do we need the tag? would this be a disc union in my book? or a protocol dispatch?
  class TypeEntry {
    int code;
    string name;
    TypeCat type;
  public:
    TypeEntry(int c, string nm, TypeCat t, TypeInfo *i) :
      code(c),
      name(nm),
      type(t),
      type_info(i) {}
    int get_code(){return code;}
    string get_name(){return name;}
    bool is_struct(){return type == TypeCat.STRUCT;}
    bool is_union(){return type == TypeCat.UNION;}
    bool is_function(){return type == TypeCat.FUNCTION;}
    bool is_built_in(){return type == TypeCat.BUILT_IN;}
    bool is_generic(){return type == TypeCat.GENERIC;}
    virtual string info();
  };
}
#endif
