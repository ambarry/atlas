#ifndef STRUCT_FIELD_H
#define STRUCT_FIELD_H

#include <string>

namespace parse {

  using std::string;

  class StructField {
    string name;
    int type_code;
  public:
    StructField(int c, string nm) : type_code(c), name(nm){}
    virtual string info();
  };
}


#endif
