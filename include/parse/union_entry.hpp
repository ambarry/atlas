#ifndef UNION_ENTRY_H
#define UNION_ENTRY_H

#include <string>
#include <unordered_map>

namespace parse {

  using std::string;
  using std::unordered_map;

  class UnionEntry : public TypeEntry {
    unordered_map<string, int> cases; // NOTE cases don't always need types
  public:
    static const int TYPELESS = -1; // consider moving to type table?
    bool has_case(string c) {
      auto search = cases.find(c);
      return search != cases.end();
    }
    bool get_type_code(string c){
      return cases[c];
    }
  };

}

#endif
