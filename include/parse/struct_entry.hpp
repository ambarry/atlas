#ifndef STRUCT_ENTRY_H
#define STRUCT_ENTRY_H

#include <string>
#include <unordered_map>

namespace parse {

  using std::string;
  using std::unordered_map;

  /*
   * Maintains info about a struct and its fields.
   * NOTE: we might want to wait on generics
   */
  class StructEntry : public TypeEntry {
    unordered_map<string, StructField*> fields;
  public:
    StructEntry(int c, string nm, TypeInfo *i) :
      base(c, nm, TypeCat.STRUCT, TypeInfo *i){}
    ~StructEntry();
    bool has_field(string fd);
    StructField* get_field(string fd); // TODO: this may need more thought w/ generics
    bool add_field(string name, int type_code); // TODO: this will need more thought w/ generics
    virtual string info();
  };
}

#endif
