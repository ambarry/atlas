#ifndef TERMINALS_H
#define TERMINALS_H

#include <string>
#include "production.hpp"
#include "token.hpp"

#define TR(classname) \
class classname : public Terminal { \
public: \
  ~classname() override {} \
  bool is_match(Token &tok) override; \
  string info() override {return #classname;} \
};

namespace parse {

  using lex::Token;
  using std::string;

  TR(Add_Op)
  TR(Amp)
  TR(And)
  TR(Arg_Cont_Op)
  TR(Arrow_Op)
  TR(Assign_Op)
  TR(Block_End)
  TR(Block_Start)
  TR(Bool_T)
  TR(Char)
  TR(Char_T)
  TR(Colon)
  TR(Comma)
  TR(Divide_Op)
  TR(Dollar)
  TR(Dot_Op)
  TR(Exp_Op)
  TR(False)
  TR(Forward_Pipe_Op)
  TR(Id)
  TR(Int)
  TR(Int_T)
  TR(L_Brace)
  TR(L_Bracket)
  TR(L_Caret)
  TR(L_Paren)
  TR(Lambda)
  TR(Match)
  TR(Mk)
  TR(Modulo_Op)
  TR(Mult_Op)
  TR(Not)
  TR(Or)
  TR(Ptr_Op)
  TR(Pipe)
  TR(R_Brace)
  TR(R_Bracket)
  TR(R_Caret)
  TR(R_Paren)
  TR(Rel_Op)
  TR(String)
  TR(String_T)
  TR(Struct)
  TR(Subtract_Op)
  TR(To_Op)
  TR(True)
  TR(Union)
  TR(When)
  TR(With)
  TR(Wildcard)
}

#endif
