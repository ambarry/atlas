// #ifndef GENERIC_STRUCT_ENTRY_H
// #define GENERIC_STRUCT_ENTRY_H
//
// #include <string>
// #include <vector>
//
// #include "struct_entry.hpp"
//
// namespace parse {
//
//   using std::string;
//   using std::vector;
//   using std::unordered_map;
//
//   class GenericStructEntry : public StructEntry {
//     vector<string> type_args;
//     // not too much has to change here, but
//     // GenericStructFields must include type args, which
//     // must be checked against this vector, but can still
//     // be stored in the same map
//   pubilc:
//     // TODO: see if we can clean this up w/out copying
//     GenericStructEntry(string nm, vector<string> args) :
//       type_args(args), base(nm){}
//     // TODO: avoid copy? maybe the std::initializers sort of deal?
//     bool add_generic_field(string nm, vector<string> args);
//     virtual string info() override;
//   };
//
// }
//
// #endif
