#ifndef TYPE_TABLE_H
#define TYPE_TABLE_H

#include <string>
#include <unordered_map>

namespace parse {

  using std::string;
  using std::unordered_map;

  /*
   * Enum to distinguish what kind of type it is
   */
  enum class TypeCat {
    BUILT_IN,
    STRUCT,
    UNION,
    FUNCTION,
    GENERIC
  };

  // /*
  //  * A simple result wrapper for add-type operations.
  //  */
  // class AddTypeResult {
  //   bool success;
  //   int code;
  // public:
  //   AddTypeResult(bool s, int c) : success(s), code(c){}
  //   bool is_succeess(){return success;}
  //   int get_code(){return code;}
  // };

  /*
   * Records all type information as it is parsed.
   * Types are stored in a vector of TypeEntry pointers.
   */
  class TypeTable {
    static const int BOOL = 0;
    static const int CHAR = 1;
    static const int INT = 2;
    static const int STRING = 3;
    static int next = 4;
    static int apply_next() {
      return next++;
    }
    vector<TypeEntry*> types;
  public:
    TypeTable();
    int add_type(TypeEntry* te);
    ~TypeTable();
    // TODO: deal w/ recursive types
    // TODO: when we access, you'll ask symbol table for id, hash to entry,
    // this will point to type code, find in type table through straigh index
  };

}

#endif
