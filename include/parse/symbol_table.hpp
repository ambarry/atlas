#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include <string>
#include <unordered_map>

namespace parse {

  using std::string;
  using std::unordered_map;

  class TableEntry {
  private:
    int type_code; // NOTE: will need trick for func types
    int line; // line number of definition
    string type_name; // NOTE: this is duplicated for convenience, may not be needed
    string name;
  public:
    TableEntry(string nm, string t_nm, int code, int ln):
      name(nm),
      type_name(t_nm),
      type_code(code),
      line(ln){}
    int get_code(){return type_code;}
    string get_name(){return name;}
    string get_type_name(){return type_name;}
  };

  class SymbolTable {
  private:
    unordered_map<string, *TableEntry> table;
    int level;
  protected:
    SymbolTable *prev_scope;
    SymbolTable *current_scope;
  public:
    SymbolTable():
      prev_scope(nullptr),
      current_scope(nullptr),
      level(0){}
    SymbolTable(SymbolTable* prev):
      prev_scope(prev),
      current_scope(nullptr),
      level(prev.get_level() + 1){}
    ~SymbolTable();
    bool insert(TableEntry* entry); // error if dup...
    TableEntry* lookup(string name); // need to backtrack scopres
    void initialize_scope(); // if current is null, become current. otherwise, setup next, prev, and change current
    void finalize_scope(); // if prev: set current to prev, delete next. else, delete current
    int get_level(){return level;}
  };

}

#endif
