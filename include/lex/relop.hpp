#ifndef RELOP_H
#define RELOP_H

#include <string>
#include "tag.hpp"
#include "token.hpp"


#define REL_OP_TABLE \
X(EQ, "EQ")         \
X(NOT_EQ, "NOT_EQ") \
X(GT, "GT")         \
X(LT, "LT")         \
X(GT_EQ, "GT_EQ")   \
X(LT_EQ, "LT_EQ")   \

namespace lex {

#define X(a, b) a,
  enum RelOpType {
    REL_OP_TABLE
  };
#undef X

  std::string get_rel_op_name(RelOpType t);


  class RelOp : public Token {
  private:
    RelOpType op_type;
  public:
    RelOp(RelOpType t, int line, int col) : Token(Tag::REL_OP, line, col), op_type(t){}
    ~RelOp(){}
    std::string info() override;
  };

}
#endif
