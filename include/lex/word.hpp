#ifndef WORD_H
#define WORD_H

#include <string>
#include <sstream>

#include "tag.hpp"
#include "token.hpp"

namespace lex {
  class Word : public Token {
  private:
    std::string lexeme;
  public:
    Word(Tag tag, std::string value, int line, int col) : Token(tag, line, col), lexeme(value){}
    Word(Tag tag, char value, int line, int col) : Token(tag, line, col) {
      std::stringstream s;
      s << value;
      lexeme = s.str();
    }
    ~Word() {}
    std::string get_lexeme() {return lexeme;}
    std::string info() override;
  };
}
#endif
