#ifndef SCAN_STATE_H
#define SCAN_STATE_H

namespace lex {
    enum class ScanState {
    START,
    WS,
    REL_OP,
    COMPOUND_OP,
    NUM,
    PUNC,
    WORD,
    MATCH,
    NO_MATCH,
    DONE,
    ERROR,
    DEDENT,
    COMMENT,
    STRING_LITERAL,
    CHAR_LITERAL
  };
}

#endif
