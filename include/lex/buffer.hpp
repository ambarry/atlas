#ifndef BUFFER_H
#define BUFFER_H

#include <istream>


namespace buf {

  const int BUFFER_SIZE = 4096;
  const int MAX_IDX = BUFFER_SIZE - 1;

  class BufferIndex {
  private:
      bool back_after_load;
      int idx;
      char* buffer;
  public:
      BufferIndex(){idx=0;}
      bool is_back_after_load(){return back_after_load;}
      void set_back_after_load(bool b){back_after_load = b;}
      int get_index(){return idx;}
      void set_index(int i){idx = i;}
      char* get_buffer(){return buffer;}
      void set_buffer(char* b){buffer = b;}
      bool is_buffer(char* b);
      char get();
  };

  class DoubleBuffer {
  private:
    std::istream& stream;
    BufferIndex current;
    BufferIndex forward;
    char* main_buffer;
    char* backup_buffer;
    void load_buffer(char buffer[]);
    void switch_buffer(BufferIndex& b);
    char advance(BufferIndex& b, bool load);
  public:
    DoubleBuffer(std::istream& input);
    ~DoubleBuffer();
    char claim();
    char advance_lookahead();
    void retreat_lookahead();
    void reset_lookahead();
    void catch_up();
    void ignore();
  };
}

#endif
