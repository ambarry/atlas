#ifndef TAG_H
#define TAG_H

#include <string>

namespace lex {

#define TYPE_TAGS             \
X(STRING_T, "STRING_T")       \
X(STRING, "STRING")           \
X(INT_T, "INT_T")             \
X(INT, "INT")                 \
X(CHAR_T, "CHAR_T")           \
X(CHAR, "CHAR")               \
X(BOOL_T, "BOOL_T")           \
X(BOOL, "BOOL")               \

#define OP_TAGS                       \
X(DOT_OP, "DOT_OP")                   \
X(ASSIGN_OP, "ASSIGN_OP")             \
X(FORWARD_PIPE_OP, "FORWARD_PIPE_OP") \
X(ADD_OP, "ADD_OP")                   \
X(MULT_OP, "MULT_OP")                 \
X(SUBTRACT_OP, "SUBTRACT_OP")         \
X(DIVIDE_OP, "DIVIDE_OP")             \
X(EXP_OP, "EXP_OP")                   \
X(MODULO_OP, "MODULO_OP")             \
X(ARROW_OP, "ARROW_OP")               \
X(PTR_OP, "PTR_OP")                   \
X(LAMBDA, "LAMBDA")                   \
X(TO_OP, "TO_OP")                     \
X(ARG_CONT_OP, "ARG_CONT_OP")         \

#define REL_TAGS              \
X(REL_OP, "REL_OP")           \

#define KEYWORD_TAGS          \
X(ID, "ID")                   \
X(AND, "AND")                 \
X(OR, "OR")                   \
X(NOT, "NOT")                 \
X(_TRUE, "TRUE")              \
X(_FALSE, "FALSE")            \
X(WHEN, "WHEN")               \
X(MATCH, "MATCH")             \
X(STRUCT, "STRUCT")           \
X(UNION, "UNION")             \
X(WITH, "WITH")               \

#define PUNC_TAGS             \
X(AMP, "AMP")                 \
X(PIPE, "PIPE")               \
X(R_PAREN, "R_PAREN")         \
X(L_PAREN, "L_PAREN")         \
X(R_BRACKET, "R_BRACKET")     \
X(L_BRACKET, "L_BRACKET")     \
X(R_BRACE, "R_BRACE")         \
X(L_BRACE, "L_BRACE")         \
X(L_CARET, "L_CARET")         \
X(R_CARET, "R_CARET")         \
X(COMMA, "COMMA")             \
X(DOLLAR, "DOLLAR")           \
X(COLON, "COLON")             \
X(WILDCARD, "WILDCARD")       \

#define BLOCK_TAGS              \
X(BLOCK_START, "BLOCK_START")   \
X(BLOCK_END, "BLOCK_END")       \
X(END_SENTINAL, "END_SENTINAL") \

#define TAG_TABLE             \
TYPE_TAGS                     \
OP_TAGS                       \
REL_TAGS                      \
KEYWORD_TAGS                  \
PUNC_TAGS                     \
BLOCK_TAGS                    \

#define X(a, b) a,
    enum Tag {
      TAG_TABLE
    };
#undef X

std::string get_tag_name(Tag tag);

}
#endif
