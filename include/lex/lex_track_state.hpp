#ifndef LEX_TRACK_STATE_H
#define LEX_TRACK_STATE_H

#include <iostream>
#include <stack>
#include <string>
#include <vector>

#include "buffer.hpp"
#include "scan_state.hpp"
#include "token.hpp"

// TODO: break out into cpp file
namespace lex {

  using buf::DoubleBuffer;
  using std::stack;
  using std::string;
  using std::vector;

  class LexTrackState {
  private:
    bool scanning;
    bool line_start;
    bool expecting_offside;
    bool immediate_offside; // HACK
    char lookahead;
    char prev_lookahead; // HACK: should actually track better in buffer
    int col;
    int prev_col; // hack for retreat_lookahead
    int line;
    int dedent_ct;
    stack<int> indents;
    string error;
    Token *tok;
    ScanState state;
    DoubleBuffer *buffer;
    void advance_col() {
      prev_col = col;
      col++;
      if (lookahead_is('\n')){
        col = 0;
      }
    }
  public:
    LexTrackState(DoubleBuffer *db): buffer(db) {
      scanning = false;
      line_start = false;
      expecting_offside = false;
      immediate_offside = false;
      col = 0;
      prev_col = 0;
      line = 1;
      dedent_ct = 0;
      indents.push(0);
      tok = nullptr;
    }
    ~LexTrackState() {}
    bool is_scanning(){return scanning;}
    bool is_line_start(){return line_start;}
    bool is_expecting_offside(){return expecting_offside;}
    bool is_immediate_offside(){return immediate_offside;}
    char get_lookahead(){return lookahead;}
    int get_col() {return col;}
    int get_line(){return line;}
    int get_dedent_ct(){return dedent_ct;}
    int get_indent(){return indents.top();}
    string get_err(){return error;}
    Token* get_tok(){return tok;}
    ScanState get_state(){return state;}
    void pop_indent(){indents.pop();}
    bool has_indents(){return !indents.empty();}

    bool lookahead_is(char c) {return lookahead == c;}
    void start_scanning(){
      scanning = true;
      set_state_start();
    }
    void stop_scanning(){
      scanning = false;
    }
    void set_line_start(bool ls){line_start = ls;}
    void expect_offside(bool is_immediate){
      expecting_offside = true;
      immediate_offside = is_immediate;
    }
    void reject_offside(){
      expecting_offside = false;
      immediate_offside = false;
    }
    void set_dedent_ct(int d){dedent_ct = d;}
    void push_indent(int i){indents.push(i);}
    void set_tok(Token *t){
      // if (tok != nullptr)
      // {
      //   delete tok;
      // }
      // no longer deleting: using tokens as pointers in parse phase
      tok = t;
    }
    void set_err(string s){error = s;}
    void set_err(const char* c){error = string(c);}
    void set_state(ScanState s){state = s;}
    void inc_line(){line++;}
    void advance_lookahead(){
      prev_lookahead = lookahead;
      lookahead = buffer->advance_lookahead();
      advance_col();
    }
    void ignore() {
      buffer->ignore(); // TODO: really don't need this unless we adjust the retreat/ prev lookahead
    }
    void retreat_lookahead(){
      // NOTE: column scheme only supports 1 retreat at a time...
      buffer->retreat_lookahead();
      lookahead = prev_lookahead;
      col = prev_col;
    }
    void set_state_match(){state = ScanState::MATCH;} // convenience
    void set_state_start(){state = ScanState::START;} // convenience
    void set_state_err(){state = ScanState::ERROR;} // convenience
    void reset_dedent() {dedent_ct = 0;}
    void inc_dedent() {dedent_ct++;}
    void matched() {buffer->catch_up();}
  };
}

#endif
