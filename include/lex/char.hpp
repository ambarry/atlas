#ifndef CHAR_H
#define CHAR_H

#include <string>

#include "tag.hpp"
#include "token.hpp"

namespace lex {

  using std::string;

  class Char : public Token {
  private:
    char val;
  public:
    Char(char c, int line, int col): Token(Tag::CHAR, line, col), val(c){}
    ~Char(){}
    char get_val() {return val;}
    string info() override;
  };
}

#endif
