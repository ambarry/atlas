#ifndef LEXER_H
#define LEXER_H

#include <vector>

#include "logger.hpp"
#include "token.hpp"

namespace lex {

  class Lexer {
  public:
    Lexer(){};
    int scan(std::istream &input, std::vector<Token*>& tokens);
  };

  // --- log helpers ---

  inline void log(std::string msg) {
    logger::log("lexer", msg);
  }

  inline void debug(std::string msg) {
    logger::debug("lexer", msg);
  }

  inline void debug(std::stringstream& msg){
    logger::debug("lexer", msg);
  }

  inline void debug(char c) {
    std::stringstream s;
    s << c << std::endl;
    logger::debug("lexer", s);
  }


  inline void error(std::string prefix, std::string msg, int line, int col) {
    std::stringstream s;
    s << prefix << " ERROR: line: " << line << ", column: " << col << std::endl;
    s << "           >> " << msg << std::endl;
    logger::error("lexer", s);
  }
}
#endif
