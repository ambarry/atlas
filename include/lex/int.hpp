#ifndef INT_H
#define INT_H

#include <string>

#include "tag.hpp"
#include "token.hpp"

namespace lex {
  class Int: public Token {
  private:
    int val;
  public:
    Int(int value, int line, int col) : Token(Tag::INT, line, col), val(value){}
    ~Int() {}
    int get_val() {return val;}
    std::string info() override;
  };
}

#endif
