#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include "tag.hpp"


// TODO: break out relop, ar_op, type, etc into subclasses
// see num strategy pg. 123 (146 pdf)

namespace lex {
  class Token {
  protected:
    Tag tag;
    int line;
    int col;
  public:
    Token(int ln, int cl) : line(ln), col(cl){}
    Token(Tag t, int ln, int cl) : tag(t), line(ln), col(cl){}
    virtual ~Token(){}
    Tag get_tag() {return tag;}
    bool is_a(Tag t){
      return tag == t;
    }
    int get_line() {return line;}
    int get_col() {return col;}
    virtual std::string info();
  };


}
#endif
