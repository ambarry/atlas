#ifndef STRING_H
#define STRING_H

#include <string>

#include "tag.hpp"
#include "token.hpp"

namespace lex {

  using std::string;

  class String : public Token {
  private:
    string val;
  public:
    String(string s, int line, int col): Token(Tag::STRING, line, col), val(s){}
    ~String(){}
    string get_val() {return val;}
    string info() override;
  };
}




#endif
