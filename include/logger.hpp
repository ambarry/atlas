#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <sstream>

namespace logger {
  using std::string;

  inline void log(string log_src, string msg) {
    std::cout << "[" << log_src << "]" << " -> " << msg << std::endl;
  }

  inline void log(string log_src, std::stringstream& msg) {
#ifdef ATLAS_LOG_ON
    log(log_src, msg.str());
#endif
  }

  inline void debug(string log_src, string msg) {
#ifdef ATLAS_DEBUG_ON
    log(log_src, msg);
#endif
  }

  inline void debug(string log_src, std::stringstream& msg) {
    debug(log_src, msg.str());
  }

  inline void error(string log_src, string msg) {
    std::cerr << "[" << log_src << "]" << " -> " << msg << std::endl;
  }

  inline void error(string log_src, std::stringstream& msg) {
    error(log_src, msg.str());
  }
}

#endif
