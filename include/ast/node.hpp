#ifndef AST_NODE_H
#define AST_NODE_H

#include <string>
#include <vector>

namespace ast {

  using std::string;
  using std::vector;

  // TODO: we need some sort of context tracking eventually,
  // for imports, naming collisions, etc.
  //
  // class Node {
  // public:
  //   virtual ~Node(){}
  //   virtual string info() = 0;
  // };
  //
  // class Expr : public Node {
  //   int return_type; // uses type code from symbol table?
  // public:
  //   virtual ~Expr(){}
  //   virtual string info() override {return "Expr";}
  //   void set_type(int t) {return_type = t;}
  //   void get_type() {return return_type;}
  // };
  //
  // /// Instance of an id being declared w/ a known type
  // /// Could also have function type, array, etc.
  // /// These may need to be separate decls, or else handle in type tables
  // class Declaration : public Expr {
  //   bool exported;
  //   string name;
  //   int id_type; // should this be a type code? or tuple, e.g. table and code? or be a type node?
  // public:
  //   Declaration(string n) : name(n){}
  //   virtual ~Declaration(){
  //     // TODO: return type should be enum for built-in "void" type
  //   }
  //   virtual string info() override {return "Declaration: " + name;} // TODO: args
  //   bool is_exported(){return exported;}
  //   void set_exported(bool b){exported = b;}
  //   string get_name(){return name;}
  // };
  //
  // // note: do we want tuple decl, or just two decls?
  //
  // class StructDeclaration : public Expr {
  //   bool exported;
  //   vector<Field> fields; // pointer to vec?
  //   // TODO:return void
  // };
  //
  // class Field : public Node {
  //   string name;
  //   int type_code;
  // };
  //
  // class UnionDeclaration : public Expr {
  //   bool exported;
  //   vector<UnionCase> cases;// pointer?
  //   // TODO: return void
  // };
  //
  // class UnionCase : public Node {
  //   string name;
  //   int type_code; // NOTE: may not have type!
  // };
  //
  // class BinaryOp : public Expr {
  //   BinaryOperator operator;
  //   Expr left;
  //   Expr right;
  // };
  //
  // class CallExpr : public Expr {
  //   int function_code; // should match symbol table entry
  //   vector<Expr> args; // it will be up to type checker to verify!
  // };
  //
  // class AssignmentExpr : public Expr {
  //   LVal lval;  // TODO: this we need to look at!!!
  //   Expr value_expr;
  // };
  //
  // class LVal : public Expr {
  //   // TODO: retrictions!
  // };
  //
  // class LamdbaExpr : public Expr {
  //   vector<string> bindings;
  //   vector<int> binding_type_codes; // TODO: pair?
  //   Expr expr;
  // };
  //
  // class MatchExpr : public Expr {
  //   Expr matchee; // TODO: need to establish this type, use in cases
  //   vector<MatchCase> cases;
  // };
  //
  // class MatchCase : public Expr {
  //
  // };

   // index access?
}

#endif
