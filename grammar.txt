## --------------------------------------------------------
# Non-terminals ---
## --------------------------------------------------------

goal
  : expr_list                                              # b_expr eventually...


## --------------------------------------------------------
## Definitions ---
## --------------------------------------------------------

definition
  : struct assign_op struct_definition                     # struct
  | union assign_op union_definition                       # union
  | type assign_op b_expr                                  # declaration

index
  : l_bracket term_expr r_bracket

id_list
  : id id_list'
  | wildcard id_list'

id_list'
  : comma id_list
  | e

struct_definition
  : block_start field_def_list block_end

field_def_list
  : amp id colon type field_def_list'

field_def_list'
  : field_def_list
  | e

union_definition
  : block_start union_case_list block_end

union_case_list
  : pipe id opt_union_type union_case_list'

union_case_list'
  : union_case_list
  | e

opt_union_type
  : colon type
  | e

## --------------------------------------------------------
## Types ---
## --------------------------------------------------------

type
  : type' type_postfix

type_list
  : type' type_list'                                          # eliminated left_recursion (be careful!)

type_list'
  : type_list
  | e

type_postfix
  | type_list to_op type'
  | to_op type'
  | e

type'
  : standard_type
  | l_paren type r_paren                                     # must use parens if you want multiple function types
  | ptr_op type'
  | list_type
  | array_type

standard_type
  : built_in_type
  | l_brace tuple_type_list r_brace                         # tuple type
  | id opt_generic_suffix                                   # TODO: alias?

built_in_type
  : char_t
  | string_t
  | int_t                                                    # TODO: floats, decimals?
  | bool_t

generic_suffix
  : l_caret type r_caret

opt_generic_suffix
  : generic_suffix
  | e

tuple_type_list
  : type tuple_type_list'

tuple_type_list'
  : comma tuple_type_list
  | e

list_type
  : dollar l_bracket type r_bracket

array_type
  : l_bracket type colon expr r_bracket

## --------------------------------------------------------
## Primary Expressions ---
## --------------------------------------------------------

b_expr
  : block_start expr_list block_end

expr_list
  : expr expr_list'

expr_list'
  : expr_list
  | e

expr
  : expr' forward_pipe_expr

expr'
  : term_expr
  | lambda_expr
  | match_expr

forward_pipe_expr
  : forward_pipe_op expr
  | e

term_expr
  : term_postfix_expr
  | term_prefix_expr

term_postfix_expr
  : term term_postfix forward_pipe_expr                     # TODO: this may need to allow list? issue w/ arg list?

term_postfix                                                # TRICKY: dot_op and index can chain...
  : argument_list                                           # function invocation
  | bool_postfix                                            # boolean expr, relop_expr
  | arithmetic_postfix
  | assign_op b_expr
  | colon definition
  | rel_postfix
  | e
  
argument_list                                               # include constructor_expr? case constructor? (keyword?)
  : term argument_list'
  | arg_cont_op term argument_list'

argument_list'
  : argument_list
  | e

term_prefix_expr
  : bool_prefix_expr                                        # logical not

term
  : term' dot_expr

term'
  : factor
  | ptr_expr                                                # ptr (TODO: op prefix instead?)
  | l_brace b_term_expr_list r_brace                        # tuple literal, or constructor w/ assignment list?
  | l_bracket b_term_expr_list r_bracket                    # array literal
  | l_paren expr r_paren                                    # TODO: may need to allow block...
  | dollar dollar_term                                      # technically, parser won't catch "anonymous" types -> will be up to next phase?

dollar_term
  : l_brace b_assignment_list r_brace                         # struct creation
  | l_bracket b_term_expr_list r_bracket                      # list literal

dot_expr
  : dot_op dot_op_postfix
  | e

dot_op_postfix
  : id dot_expr
  | index dot_expr                                          # TODO: slice

factor
  : char
  | string
  | int
  | bool
  | id
  | wildcard

b_term_expr_list
  : block_start term_expr_list block_end
  | term_expr_list

term_expr_list
  : expr term_expr_list'
  | e

term_expr_list'
  : comma term_expr_list
  | e

b_assignment_list
  : block_start assignment_list block_end
  | assignment_list

assignment_list
  : id colon expr assignment_list'

assignment_list'
  : comma assignment_list
  | e

## --------------------------------------------------------
## Lambda Expressions ---
## --------------------------------------------------------

lambda_expr
  : lambda binding_list arrow_op b_expr

binding_list
  : id binding_list'

binding_list'
  : binding_list
  | e

## --------------------------------------------------------
## Pointer Expressions ---
## --------------------------------------------------------

ptr_expr
  : ptr_op expr


## --------------------------------------------------------
## Boolean Expressions ---
## --------------------------------------------------------

bool
  : true
  | false

bool_postfix
  : and expr
  | or expr

rel_postfix
  : rel expr

rel
  : l_caret
  | r_caret
  | rel_op

bool_prefix_expr
  : not expr


## --------------------------------------------------------
## Arithmetic Expressions ---
## --------------------------------------------------------

arithmetic_postfix
  : arithmetic_op expr

arithmetic_op                                               # TODO: precedence???
  : add_op
  | subtract_op
  | mult_op
  | divide_op
  | modulo_op
  | exp_op


## --------------------------------------------------------
## Pattern Matching Expressions ---
## --------------------------------------------------------


match_expr
  : match expr with block_start rule_list block_end

rule_list
  : rule rule_list'

rule_list'
  : rule_list
  | e

rule
  : pipe pattern pattern_guard arrow_op b_expr

pattern
  : factor pattern_factor_postfix                           # const or ident pattern TODO: adjust this!!
  | collection_pattern
  | tuple_pattern                                           # TODO: struct / record pattern?

pattern_guard
  : when expr
  | e

pattern_factor_postfix
  : colon colon pattern                                     # cons pattern
  | pipe pattern                                            # disjoint pattern
  | amp pattern                                             # conjunctive pattern
  | factor                                                  # union case w/ value, e.g. red 50, blue _
  | e                                                       # binding, const, or union case depending on lookup / use

# NOTE: pattern within must match type of element, but otherwise
# may still be pattern:

collection_pattern
  : l_bracket pattern_list r_bracket                        # array (may be empty)
  | dollar l_bracket pattern_list r_bracket                 # list (may be empty)

tuple_pattern
  : l_brace pattern_list r_brace

pattern_list
  : pattern pattern_list'
  | e

pattern_list'                                               # TODO: this allows trailing comma, which is inconsistent
  : comma pattern_list
  | e

## --------------------------------------------------------
# Terminals ---
## --------------------------------------------------------

add_op
amp
and
arrow_op
assign_op
block_end
block_start
bool_t
char
char_t
colon
comma
divide_op
dollar
dot_op
exp_op
false
forward_pipe_op
goal
id
int
int_t
l_brace
l_bracket
l_paren
lambda                                                      # .\
match
modulo_op
mult_op
not
or
ptr_op                                                      # @
pipe
r_brace
r_bracket
r_paren
rel_op
string
string_t
struct
subtract_op
to_op
true
union
when
wildcard                                                    # _


## --------------------------------------------------------
# TODO: Remaining items ---
## --------------------------------------------------------

# wave two:
# piping
# modules
# forward pipe? or is this an inline func?
# map literals?
# array slicing
# aliases
# ops
# ranges
# write tests for everything!!!
