/* This file is for testing random things, when
 * it's more convenient than looking them up.
*/


#include <iostream>
#include <string>
#include <unordered_map>

using std::endl;
using std::cout;
using std::cin;
using std::string;
using std::unordered_map;

void map_test();

int main() {
  cout << "SCRATCH BEGIN" << endl;
  map_test();
}

void map_test(){
  unordered_map<string, int> map;
  map["one"] = 1;
  auto search = map.find("one");
  if (search != map.end()) {
    cout << "Found it!" << endl;
  }
  else {
    cout << "Not in the map!" << endl;
  }
}
