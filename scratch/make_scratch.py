# This is a script to make Atlas.
# I hate Makefiles so hopefully
# this will do some good.

import os

compiler = "g++"
cflags = "-std=c++11 -Wall -Werror"
src = "scratch.cpp"
obj = "scratch.o"
exe  = "scratch"

def make_objects():
    cmd = "{0} {1} -c {2}".format(compiler, cflags, src)
    print "Compiling source: \n" + cmd
    res = os.system(cmd)
    return os.WEXITSTATUS(res)

def make_exe():
    cmd = "{0} {1} -o {2} {3}".format(compiler, cflags, exe, obj)
    print "Compiling exe: \n" + cmd
    res = os.system(cmd)
    return os.WEXITSTATUS(res)

def clean():
    print "Cleaning...\n"
    os.remove(obj)

def make():
    res = make_objects()
    if res == 0:
        print "Compiled source successfully!!!\n"
        res = make_exe()
        if res != 0:
            print "\nError compiling {0} - exit code: {1}".format(exe, res)
        else:
            print "\nCompiled {0} successfully!!!\n".format(exe)
        clean()
        print "Done!\n"
    else:
        print "Error compiling source - exit code: {0}".format(res)

if __name__ == '__main__':
    make()
