# .\TLAS compiler

- note: consider apollo (space connotation, but also archery)
- -> (arrow) and |> (bow-ish)
- would seem less arrogant than (weight of world)
- .lo


## gcc (libcpp)

- init.c
- symtab.c
- symbol table is a cpp_hash_table
  + entry is a cpp_hashnode
  + cpp_lookup(reader, name, len)
  + indentifiers.c, lex.c,directives.c, pch.c
- they init built-ins
  + they use bitflags to indicate NODE_BUILTIN, e.g., warns, etc.


## ast

- build out nodes
  + Node (interface)
  + Expr
  + statement? really, everything is an expr...
    - maybe separate assignment?
    - or we consider call and return statements, even when pure
    - outermost is list (will have module, imports), list of statements
    - TODO: main or entrypoint tag / test
  + Declaration
  + Struct
  + Union
  + Field
  + Id
  + LambdaExpr
    - look at C++ lambdas...
  + MatchExpr
  + BlockExpr? w/ return type? (NOTE: can be unit/void?...)
  + CallExpr w/ Args
    - can combine in arg continuation, don't
  + BinaryExpr
  + Types (built-ins)
  + Index / Slice / Range
  + PipeExpr? (or just do call exprs and change around args...)
  + handle arithmetic precedence?
  + eventually, a Bad indicator, for when you can't resolve
- ORRR do we go Elixir, just tuples of ops, args, etc.
- the draw there is that we don't invest too heavily in OOP
  + will be rewriting anyway
  + what's the best functional way to do this? assign a functor?
- opportunities to check, once LHS vs RHS is established
  + if we get a declare op, can check LHS args, add to symbol table
  + if we get assignment, can check LHS args to make sure they're in table
  + if on RHS, can check for underscores
- can also check scope
  + if not outermost scope level, forbid types, e.g.
  + if outermost level and pure expr, warn when result isn't used
    - could do for all, really
- maintain exports
  + either a keyword or funcs w/out underscores Python style
  + build up an export list
- maintain unresolved, once you have imports...


## RTL

- register transfer language, 3-addr style
- see C book

## type checker

- load up built-ins
- wait for ast
- traverse depth-first
- compare nodes to arg nodes, assingment to types, etc.

## nxt

- need to store value, have code-snippet / action for syntax-directed translation
  + this should occur as productions expand...
  + we can trickle the values down
  + productions may need to be templates? or hold a template?
  + or do we have a separate stack or way to track these values?
- may also just hang onto the nodes and resolve after??
- could separate LHS, RHS parses, and even productions
  + would make catching errors in parser easier
  + but how to distinguish? do we assume lhs after b, but defer to rhs if it fails?
- build and check symbol tables as you go
- build and check type tables
  + load with defaults...
- issue a warning for unbound expressions
  + this could include something like an unmatched let
  + it will also include any random expressions at top level
    - these will be implicitly ignored in F# fashion
- test associativity?
- type check: now allowing everything on either half
  - this means that during type check (or before...)
    + need to special case underscores on rhs
    + need to force ids and underscores on lhs
    - could still have something like "true" in outer scope: not ideal
  - should separate statements?...
- for infix ops too? do we want any?
- aliases and typing funcs to reduce boilerplate and enforce type
- c++ issue w/ enum and initialization? why sometimes default, sometimes start?
  + inconsistent behavior...
- write lexer tests!!!
- loop syntax
- need a single-line if style match?


## desired additional feature list

- floats
- decimals
- set literal?
- map literal?
- initializers
  + something protected that can efficiently initialize a record
  + should be very specific
  + should allow composition, clean and concise code separation
- type transform (is this a good idea? seems hacky...)
  + could do w/ unknown derived
  + not really needed if we have no real oop though...
- promises / channels / inboxes / async
  + this requires a threadpool executor though
  + if we had a good channel mechanism (which also does...) then you could just send back once finished
  + I like message passing better, so this may be the way to go
  + otherwise, need something clever
    - parallel code...
- variadic type for custom list?
  + could be useful, but also abused... don't really want to pass to functions...
  + need to think about this more
- concepts?
  + read up if that's what you think it is
  + idea is to say, mark something that must or can't perform io, modify stuff, etc.
  + could guard funcs with these, then protocols would feel a little safer
    - guarding behavior, rather than type?
  + do we need templates / generics?


---

## random

explicit and readable
control
  - should opt in for something low level, raw pointer, etc. default to refs?
easy to reason about
avoid bad formatting
maintainable
promote good patterns (e.g. unsused = error)
AST as expressable in the language (metaprogramming)
  - may be rolling back on that... (save for Apollo?)
  - or do it! trust the programmer!!!
SIMD?

if half the fun is defining a curried function on the fly,
then we need to be able to do this really easily
and ideally it will be the same format as a named func


## polymorphism

state: basically a record / object / struct / named tuple
 - no methods
 - named parameters
 - all public
 - inheritance (doesn't really hurt, since just tagged fields)
   + may want to roll back (conflicts, e.g. two types with same named prop, different types)
 - interfaces (in case shared prop, but not related entity.
   - do interfaces solve or replace generics?
   - could, but lose type info, like hashtable of x, list x
   - template classes are important, stack, queue, to api controller

this solves:

print_name: inamed -> void =
  n ->
    print "The name is: " + n.name (etc.)


2nd problem: behaviors / protocols

e.g.

run_all: list runnable -> void =
  runnys ->
    runnys | > List.each(r => run r)  
    # how do we know we have a "run" that takes r?
    # is this really a compelling use case?
    # what about something like size or len or to_string or enumerable
    # or would generics be better? (but they imply functional interfaces)


this does seem valuable... but also leads to many problems


## grammar: loops and imperative statements vs recursion?

foreach (int i in list) {stuff(list[i])}
for (int i = 0; i < list.length; i++) {}
for (auto i : list.iter()) {}
list |> Enum.each(fn x -> stuff(x) end)
list |> Seq.iter(fun x -> stuff x)
for i in list:
  stuff(i)
lazy combinators?

## lang: mutability and pointers and concurrency?

* how do we want to handle?
* nothing crazy like a borrow system, but isolating side-effects is ideal
* need to solve the problem of state
* explicitly mutate a pointer?
* only reason should be to save mem
* be nice to pass by ref
* like immutability, but annoying to change parts, have to create whole new object w/ one thing changed, cascade up
* it'd be cool to do this w/ same mem though, then no worry
* so no:

example of destroying an object from somewhere else...
or adding to an array from outside the class, can't tell how it's loaded...

* seq iter (lazy) beats the while or for loop for ranges
* but when searching, checking value, basically stuck with recursion
* refs are an issue
* mutation isn't the issue, but isolating it is
  - can't have flags getting set in random places
  - although, since you're forced to pass state along, it's a little clearer




## concurrency

* simple spawning of processes (although stuck with threads unless you want a runtime...)
* could be a cool language in which to implement a runtime though
* maybe this is the low-level version, on top of which we build things
* e.g. this is Atlas, and later Apollo could be the high level glory with some sweet FFI abilities













  lexer

* include line and column number in token for parser error messages
[x] string literals (should we move to parser though? esp. w/ interpolation...)
  - could have a separate token for string_w_interpolation
  - could also just look for curly braces, add inner identifiers to the String token for the parser to look for and substitute
* char literals
* for lexer or parser:
  - tuple literals?
  - list literals?
  - array literals?
* string interpolation
* floats, decimals, etc.


## parser

* symbol table
  * reserved words -> add to symbol table early
  * identifiers -> put in symbol table if not there
* solidify features that you want (std lib can wait a little, channels, etc)
* but compilation / package system and vcs friendliness should be considered
* ease of db driver should be considered
* ease of TCP / UDP modules (e.g. just send over a port)


## the runtime ??

* NOTE: starting to turn against this...
* actor model, message passing
* no shared state, but refs / ownership / move semantics?
* don't compile if leak
* the runtime is more of a scheduler than a real runtime
* NO GC -> this is a must
* maybe no scheduler: or library, rather than runtime...

## names
* atlas
* arc
* taft
* wildcat

## overall language goals

### SHAPE

* vertical
* elegant
* favor indentation

### fun!

* have a sense of humor!
* deranged!!!
* everyone has gone MAD
* atlas may not be the right name...
* fun operators
  * streams?
* crafty string interpolation
* currying!

### extensible?

* lots of meta programming
* hygienic macros
* easy DSLs
* easy to write client / drivers!
  * should be able to express the client protocol in a very close but safe manner
    * through macros, quotations, op overloads?

### control


### organization/ project structure / tooling

* should be easy to find things, not have to jump around files / folders too much
  * but should be easy to keep files clean
    * if you have to scroll in a mega file, it's not working...
  * two sides of something like a header file...
  * should be EXPLICIT where something is -> partial classes are bad this way...
* but do NOT dictate / flexible!!
  * in what you can do
  * in how you can do it
  * in project structure, etc.
* easy configuration!!!
* good, EASY tooling
  * builds should be a breeze!!
  * package deps
  * unit testing
* IOC
  * maybe configurable, like Elixir
* easy for vcs!!! (diffs, renames, etc.)
* easy for package manager


### concurrency / channels

* need to have own scheduler / multiplexer as part of runtime
* goroutines are interesting, but shared memory space doesn't seem good...
  * really don't want to deal w/ synching
  * alt. is a lot of copying for messages...
* whatever your "thread" / "process" is, needs an identity that you can register...
* would there be a way to send messages w/ an ownership system?
  * hard to do across nodes though, but locally it makes sense
  * would have to make pointers non-copyable (really, non-operable at all), not sure how this could be acheived...
  * if there's no GC, then in a way we eliminate the shared-memory issue, although it feels like that may need more thought
  * who's responsibility is it to de-allocate the memory now? the receiever? what if it ends before it receives the message? then we have a leak!
    * the global manager could assume responsibility for failed messages, or at least keep refs and give you  some access?
  * if this works though, could be REALLY powerful -> awesome safe concurrency but low-level memory management, no GC pause, no copy time / mem use for big messages
  * diff node, must copy
  * run-time might be really fun :)
* keep web in mind

### straightforward

* friendly compiler errors!
  * descriptive!!!
  * don't have to fight the compiler! he is your friend!
* Least Astonishment!
* documentation as a feature?


## syntax features

* blocks -> indentation over do/end or {}
* get strings right
* easy string interpolation
* easy serialization and serialization extensions
* macros, or something like that...
  * maybe a simpler code_gen sort of deal? just spits out, sort of variable string interpolation...
  * or do a real quote / unquote sort of deal...
  * note: COMPILE-TIME: can't do in a loop, for example
    * do we need a run-time? or accept funcs in it? more useful...
    * then you need the quote / unquote...
* functional
  * piping
  * pattern matching
    * or types, discriminated unions
  * partial application?
  * currying!!!
  * immutability???
  * tail-recursion
* any OOP features?
  * leaning towards no inheritance, favor composition...
  * but keep interface... see below
* some sort of behaviors / interfaces
  * would be nice to make IOC / DI really really easy?
  * maybe ditch normal OOP inheritance -> ends up messy, favor composition if you go OOP
* static typing
* I like functional style, but I like classes over weird record / structs
  * really, just like named access
  * maybe access only sort of classes? basically a record?
* don't just emulate features!!!
  * how do YOU want it to work?
* something like generics, but it'd be nice to be a little slicker

## Good practices

* no statics
* no globals
