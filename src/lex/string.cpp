#include <sstream>
#include <string>
#include "string.hpp"
#include "tag.hpp"


namespace lex {
  using std::stringstream;
  using std::string;

  string String::info() {
    stringstream s;
    s << get_tag_name(tag) << ": " << val;
    return s.str();
  }
}
