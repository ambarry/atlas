#include <cstdio>
#include <istream>
#include <iostream>
#include "buffer.hpp"


namespace buf {

  bool BufferIndex::is_buffer(char* b){
    return buffer == b;
  }

  char BufferIndex::get(){
    char c = buffer[idx];
    idx++;
    return c;
  }

  void DoubleBuffer::load_buffer(char buffer[]){
    stream.read(buffer, MAX_IDX); // read 1 less, room for sentinel
    std::streamsize bytes_read = stream.gcount();

    // load sentinels into buffer
    buffer[bytes_read] = EOF;
    buffer[MAX_IDX] = EOF;
  }

  /**
   * Loads the two buffers as much as possible from the stream.
   */
  DoubleBuffer::DoubleBuffer(std::istream& input) : stream(input) {
    // TODO: handle errors on stream
    main_buffer = new char[BUFFER_SIZE];
    backup_buffer = new char[BUFFER_SIZE];
    load_buffer(main_buffer);
    current.set_buffer(main_buffer);
    forward.set_buffer(main_buffer);
  }

  DoubleBuffer::~DoubleBuffer(){
    delete main_buffer;
    delete backup_buffer;
  }


  /**
   * Returns the char pointed to by a BufferIndex and advances the index.
   * If the end of a buffer was read, we advance to the next buffer.
   * Optionally, we may load the next buffer when advancing.
   */
  char DoubleBuffer::advance(BufferIndex& b, bool load){
    char c = b.get();
    if (c == EOF && b.get_index() == BUFFER_SIZE){
      // If not, we've reached true end. But if yes, advance to next buffer.
      switch_buffer(b);
      // only load if forward, not current, and we didn't move back after load
      if (load && !b.is_back_after_load()){
        load_buffer(b.get_buffer());
      }
      b.set_back_after_load(false);
      b.set_index(0);
      c = b.get(); // read the first of next buffer
    }
    return c;
  }

  /**
   * Advances the current buffer to collect the lexeme
   * one char at a time.
   */
  char DoubleBuffer::claim(){
    return advance(current, false);
  }

  /**
   * Advances the current buffer but ignores the char.
   */
  void DoubleBuffer::ignore(){
    advance(current, false);
  }

  /**
   * Advances the forward buffer to determine the current
   * lexeme.
   */
  char DoubleBuffer::advance_lookahead(){
    return advance(forward, true);
  }

  /**
   * Moves the forward index backward.
   */
  void DoubleBuffer::retreat_lookahead(){
    int i = forward.get_index();
    if (i == 0){
      // move to end of previous buffer
      switch_buffer(forward);
      forward.set_index(MAX_IDX);
      forward.set_back_after_load(true);
    }
    else {
      // just move back a place
      forward.set_index(i-1);
    }
  }

  /**
   * Resets the lookahead to the current position.
   */
  void DoubleBuffer::reset_lookahead() {
    forward.set_index(current.get_index());
    // if we moved back, don't want to reload
    char* cbuf = current.get_buffer();
    if (cbuf != forward.get_buffer()) {
      forward.set_buffer(cbuf);
      forward.set_back_after_load(true);
    }
  }

  /**
   * Catches the current pointer up to the lookahead.
   */
  void DoubleBuffer::catch_up() {
    current.set_index(forward.get_index());
    current.set_buffer(forward.get_buffer());
  }

  /**
   * Switches the buffer that to which a BufferIndex points.
   */
  void DoubleBuffer::switch_buffer(BufferIndex& b){
    char* buf_to_set = b.is_buffer(main_buffer)
      ? backup_buffer
      : main_buffer;
    b.set_buffer(buf_to_set);
  }

}
