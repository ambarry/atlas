#include <cctype>
#include <cstdlib>
#include <iostream>
#include <map>
#include <regex>
#include <sstream>
#include <stack>
#include <string>
#include <unordered_set>
#include <vector>

#include "buffer.hpp"
#include "char.hpp"
#include "int.hpp"
#include "lexer.hpp"
#include "lex_track_state.hpp"
#include "logger.hpp"
#include "relop.hpp"
#include "scan_state.hpp"
#include "string.hpp"
#include "tag.hpp"
#include "token.hpp"
#include "word.hpp"

using buf::DoubleBuffer;
using std::cout;
using std::endl;
using std::istream;
using std::map;
using std::stack;
using std::string;
using std::vector;
using std::stringstream;

/**
 * The lexer.
 * Breaks source into tokens.
 * Handles any istream, be it a filestream, stringstream, etc.
 * Reports errors on any invalid tokens for the grammar.
 *
*/

namespace lex {

  bool is_valid_word_start(char c) {
    return (std::isalpha(c) || c == '_');
  }

  bool is_valid_word_char(char c) {
    return (std::isalnum(c) || c == '_');
  }

  bool is_indent_char(char c) {
    return c == '\t' || c == ' ';
  }

  bool is_char_lit_marker(char c) {
    return c == '\'';
  }

  bool is_string_lit_marker(char c) {
    return c == '\"';
  }

  bool is_offside_starter_punc(char c){
    return c == '=' || c == '{' || c == '[' || c == '(';
  }

  string gen_column_err(const char* msg, int col) {
    stringstream ss;
    ss << msg << col;
    return ss.str();
  }

  // TODO: could return LexTrackState to make this more functional
  // better yet, return the next func, or null when done
  void consume_indent(LexTrackState& lts) {
    int spaces = 0;
    int tmp;
    bool working = true;
    while (working) {
      if (lts.lookahead_is(' ')) {
        spaces++;
      }
      else if (lts.lookahead_is('\t')) {
        // going to follow Python / UNIX style, convert tabstops of 8 spaces
        tmp = spaces + 8;
        spaces = tmp - (tmp % 8);
      }
      else {
        working = false;
      }
      if (working) {
        lts.advance_lookahead();
      }
    }

    // if this line was a comment, we don't care about indents or dedents
    // just set state to process comment instead!
    if (lts.lookahead_is('#')) {
	    lts.set_state(ScanState::COMMENT);
	    return;
    }

    lts.retreat_lookahead();

    // check spaces against stack
    int lvl = lts.get_indent();

    // if equal, either expecting new indent or we're good
    if (spaces == lvl) {
      if (lts.is_expecting_offside()) {
        lts.set_err(gen_column_err("Expected indentation at column: ", spaces));
        lts.set_state_err();
      }
      else {
        lts.set_state_start(); // still in same level, nothing to do
      }
    }

    // if greater, either expecting new indent or unnecessary
    else if (spaces > lvl) {
      if (lts.is_expecting_offside()) {
        lts.push_indent(spaces);
        Token *t = new Token(Tag::BLOCK_START, lts.get_line(), lts.get_col());
        lts.set_tok(t);
  	    lts.reject_offside();
  	    lts.set_state_match();
      }
      else {
        lts.set_err(gen_column_err("Unexpected indentation at column: ", spaces));
  	    lts.set_state_err();
      }
    }

    // if less, ether a valid dedent, multiple, or error
    else {
      if (lts.is_expecting_offside()) {
        lts.set_err(gen_column_err("Expected indentation at column: ", spaces));
        lts.set_state_err();
      }
      else {
        // dedent until we have one on the stack (or error)
        lts.reset_dedent();
        while (spaces != lvl){
          lts.pop_indent();
          lts.inc_dedent();
          if (lts.has_indents()) {
            lvl = lts.get_indent();
          }
          else {
            break; // TODO: HACK
          }
        }
        if (spaces == lvl) {
          lts.set_state(ScanState::DEDENT); // all good : track dedents
        }
        else {
          lts.set_err(gen_column_err("Incorrect dedentation at column: ", spaces));
          lts.set_state_err(); // TODO: error codes for easy testing?
        }
      }
    }
  }

  void consume_string_literal(LexTrackState& lts) {
    // TODO: deal w/ escapes, interpolation? very large strings
    // TODO: may want to keep count and handle, < MAX_STRING_LITERAL?

    bool closed = false;
    stringstream builder;
    while (true) {
      lts.advance_lookahead();
      if (lts.lookahead_is('\n')) { // TODO: check this
        break;
      }
      if (is_string_lit_marker(lts.get_lookahead())) {
        closed = true;
        break;
      }
      builder << lts.get_lookahead();
    }

    // TODO: should we let newlines be in strings always?
    // literals, like back-ticked version? why the hell not?
    if (closed) {
      lts.set_tok(new String(builder.str(), lts.get_line(), lts.get_col()));
      lts.set_state_match();
    }
    else {
      lts.set_err("String format: expected closing \"");
      lts.set_state_err();
    }
  }

  void consume_char_literal(LexTrackState& lts) {
    // TODO: deal w/ escapes?
    lts.advance_lookahead();
    char c = lts.get_lookahead();
    lts.advance_lookahead();
    char close_quote = lts.get_lookahead();
    if (is_char_lit_marker(close_quote)) {
      lts.set_state_match();
      lts.set_tok(new Char(c, lts.get_line(), lts.get_col()));
    }
    else {
      lts.set_err("Char format: expected closing \'");
      lts.set_state_err();
    }
  }

  void consume_punc(LexTrackState& lts) {
    static map<char, Tag> puncs {
      {'=', Tag::ASSIGN_OP},
      {'+', Tag::ADD_OP},
      {'-', Tag::SUBTRACT_OP},
      {'*', Tag::MULT_OP},
      {'/', Tag::DIVIDE_OP},
      {'.', Tag::DOT_OP},
      {'%', Tag::MODULO_OP},
      {'@', Tag::PTR_OP},
      {'|', Tag::PIPE},
      {'&', Tag::AMP},
      {'(', Tag::L_PAREN},
      {')', Tag::R_PAREN},
      {'[', Tag::L_BRACKET},
      {']', Tag::R_BRACKET},
      {'{', Tag::L_BRACE},
      {'}', Tag::R_BRACE},
      {',', Tag::COMMA},
      {'$', Tag::DOLLAR},
      {':', Tag::COLON},
      {'<', Tag::L_CARET},
      {'>', Tag::R_CARET},
      {'_', Tag::WILDCARD},
      {'^', Tag::ARG_CONT_OP}
    };

    if (puncs.find(lts.get_lookahead()) == puncs.end()){
      lts.set_state(ScanState::WORD);
    }
    else {
      lts.set_tok(new Token(puncs[lts.get_lookahead()], lts.get_line(), lts.get_col()));
      lts.set_state_match();
      if (is_offside_starter_punc(lts.get_lookahead())) { // TODO: could clean this...
        bool immed = lts.lookahead_is('=');
        lts.expect_offside(immed);
      }
    }
  }

  void consume_rel_op(LexTrackState& lts) {
    RelOpType op_type;
    bool matched = false;
    char cur = lts.get_lookahead();
    lts.advance_lookahead();

    // editing: need to save <> for the parser? or are we ditching these...
    // TODO: currently an issue! or will be when we have real
    // generics or templates? will we?

    switch (cur) {
      case '!':
        if (!lts.lookahead_is('='))
          break;
        op_type = RelOpType::NOT_EQ;
        matched = true;
        break;
      case '=':
        if (!lts.lookahead_is('='))
          break;
        op_type = RelOpType::EQ;
        matched = true;
        break;
      case '>':
        if (!lts.lookahead_is('=')) {
          break;
        }
        op_type = RelOpType::GT_EQ;
	      matched = true;
        break;
      case '<':
        if (!lts.lookahead_is('=')) {
          break;
        }
        op_type = RelOpType::LT_EQ;
        matched = true;
        break;
      default:
        break;
    }

    if (matched) {
      lts.set_tok(new RelOp(op_type, lts.get_line(), lts.get_col()));
      lts.set_state_match();
    }
    else {
      lts.retreat_lookahead();
      lts.set_state(ScanState::COMPOUND_OP);
    }
  }

  void consume_compound_op(LexTrackState& lts) {
    bool matched = false;
    char cur = lts.get_lookahead();
    lts.advance_lookahead();
    Tag tag;

    // TODO: the combo assignment sort of ops, e.g. +=
    // TODO: can we allow custom? how would that work? probably later, but may have implications here regarding lookahead...
    // might need to reserve a few, set as custom op

    switch (cur) {
      case '|':
        if (lts.lookahead_is('>')) {
          tag = Tag::FORWARD_PIPE_OP;
          matched = true;
        }
        break;
      case '*':
        if (lts.lookahead_is('*')) {
          tag = Tag::EXP_OP;
	        matched = true;
        }
        break;
      case '-':
        if (lts.lookahead_is('>')) {
          tag = Tag::ARROW_OP;
          matched = true;
          lts.expect_offside(true);
        }
	      break;
      case '=':
        if (lts.lookahead_is('>')) {
          tag = Tag::TO_OP;
          matched = true;
        }
        break;
      case '.':
	      if (lts.lookahead_is('\\')) {
          tag = Tag::LAMBDA;
	        matched = true;
        }
	      break;
      // case '\\':
    	//   if (lts.lookahead_is('\\')) {
	    //     tag = Tag::TO_OP;
	    //     matched = true;
	    //   }
      default:
        break;
    }

    if (matched) {
      lts.set_tok(new Token(tag, lts.get_line(), lts.get_col()));
      lts.set_state_match();
    }
    else {
      lts.retreat_lookahead();
      lts.set_state(ScanState::NUM);
    }
  }

  Tag get_word_tag(string word) {
    static map<string, Tag> keywords = {
      {"int", Tag::INT_T},
      {"string", Tag::STRING_T},
      {"char", Tag::CHAR_T},
      {"bool", Tag::BOOL_T},
      {"true", Tag::_TRUE},
      {"false", Tag::_FALSE},
      {"match", Tag::MATCH},
      {"when", Tag::WHEN},
      {"struct", Tag::STRUCT},
      {"union", Tag::UNION},
      {"not", Tag::NOT},
      {"and", Tag::AND},
      {"or", Tag::OR},
      {"with", Tag::WITH}
    };
    if(keywords.find(word) != keywords.end())
      return keywords[word];
    else
      return Tag::ID;
  }

  void consume_num(LexTrackState& lts) {
    // TODO: floats, exps, etc.
    int nxt;
    int tot = 0;
    while (std::isdigit(lts.get_lookahead())) {
      nxt = lts.get_lookahead() - '0'; // convert to int
      tot = 10 * tot + nxt;
      lts.advance_lookahead();
    }
    lts.retreat_lookahead();
    lts.set_tok(new Int(tot, lts.get_line(), lts.get_col()));
    lts.set_state_match();
  }

  /*
   * Adds a dedent for all remaining indents in the stack.
  */
  void close_dedents(vector<Token*>& tokens, LexTrackState& lts) {
    // add a dedent for all remaining in indents stack
    while (lts.has_indents() && lts.get_indent() != 0)
    {
      Token* t = new Token(Tag::BLOCK_END, lts.get_line(), lts.get_col());
      tokens.push_back(t);
      debug(t->info());
      lts.pop_indent();
    }
  }

  void consume_word(LexTrackState& lts) {
    stringstream builder;
    while (is_valid_word_char(lts.get_lookahead())){
      builder << lts.get_lookahead();
      lts.advance_lookahead();
    }
    lts.retreat_lookahead();
    string wrd = builder.str();
    Tag tag = get_word_tag(wrd);
    Word *w = new Word(tag, wrd, lts.get_line(), lts.get_col());
    lts.set_tok(w);
    lts.set_state_match();
    if (tag == Tag::WITH) {
      lts.expect_offside(true);
    }
  }

  void consume_comment(LexTrackState &lts) {
    // ignore all chars in the comment
    do {
      lts.advance_lookahead();
    } while (!lts.lookahead_is('\n'));
    lts.retreat_lookahead(); // put the newline back on
    lts.set_state_start();
  }


  /*
   * The main lexer scan. Runs the state machine.
   * TODO: write tests!
   * TODO: the line / arg conitnuation
   * TODO: need to break this out into common func w/ same args, just pass pointer to next func at end, rather than state
   * Remove the refs
   * Build a class for all the params
   * Return a new class w/ the next setup
   * This is what we maintain
   */
  int Lexer::scan(istream& input, vector<Token*>& tokens) {
    log("Lexical analysis beginning...");
    DoubleBuffer *buffer = new DoubleBuffer(input);
    LexTrackState lts(buffer);
    lts.start_scanning();

    while (lts.is_scanning()) {
      switch (lts.get_state()) {
        case ScanState::START:
	        lts.advance_lookahead();
          lts.set_state((lts.lookahead_is(EOF)) ? ScanState::DONE : ScanState::WS);
          break;
        case ScanState::WS:
          if (lts.lookahead_is('\n')) {
            lts.set_line_start(true);
            lts.inc_line();
          }
          if (is_indent_char(lts.get_lookahead()) && lts.is_line_start()) {
	          // Calculate the column number of first white space, considering unix tabstop convention (though tabs really shouldn't be used...)
	          // if the first whitespace was a comment though, put that in state instead, and internally, ignore those indents/ dedents
            consume_indent(lts);
            lts.set_line_start(false);
          }
          else if (std::isspace(lts.get_lookahead())) {
            // ignore the rest of whitespace chars - may need else for line_start to work properly
            lts.set_state_start();
	          lts.ignore();
          }
          else {
            // TODO: need to mark if expecting col!
            if (lts.is_line_start()) {
              close_dedents(tokens, lts);
            }
            lts.set_line_start(false);
            lts.set_state(ScanState::REL_OP);
            // if we're expecting an offside char, and the next char (the lookahead) isn't whitespace,
            // then by definition it must be the next offside col (NOTE: UNLESS IT IS A COMMENT!!!)
            // really though, we don't normally care, as the indents would have tracked it
            // the only reason we care is that it may have followed an '=' or '->', followed by a space.
            // we can insert a fake block here if need be, as it may become a real block
            // or we just don't really care outside of lexical analysis
            // however, this duplication is not great...

            // TODO: only expect for =,->, or container start followed by newline
            // the newline instance is already handled
            // just handle = and arrow op! And with!!!
            // orrrrrr don't? expr then wouldn't necessarily have block but you could
            // reuse the arow op...
            // HACK: for comments...
            if (!lts.lookahead_is('#')){
              if (lts.is_expecting_offside() && lts.is_immediate_offside()){
                lts.push_indent(lts.get_col() - 1); // TODO: may need to check...
                Token* b = new Token(Tag::BLOCK_START, lts.get_line(), lts.get_col()); // TODO: wrap this...
                tokens.push_back(b);
                debug(b->info());
              }
              lts.reject_offside();
            }
          }
          break;
        case ScanState::REL_OP:
          consume_rel_op(lts);
          break;
        case ScanState::COMPOUND_OP:
          consume_compound_op(lts); //TODO: need to adjust the expecting flag, pass it along everywhere, track the indent!!
          break;
        case ScanState::NUM:
          if (std::isdigit(lts.get_lookahead())){
              consume_num(lts);
          }
          else {
            lts.set_state(ScanState::PUNC);
          }
          break;
        case ScanState::PUNC:
          consume_punc(lts); // TODO: need to adjust the expecting flag, pass it along everywhere, track the indent!!!
          break;
        case ScanState::WORD:
          if (is_valid_word_start(lts.get_lookahead())) {
            consume_word(lts);
          }
          else {
            lts.set_state(ScanState::STRING_LITERAL);
          }
          break;
        case ScanState::STRING_LITERAL:
          if (is_string_lit_marker(lts.get_lookahead())) {
            consume_string_literal(lts);
          }
          else {
            lts.set_state(ScanState::CHAR_LITERAL);
          }
          break;
        case ScanState::CHAR_LITERAL:
          if (is_char_lit_marker(lts.get_lookahead())) {
            consume_char_literal(lts);
          }
          else {
            lts.set_state(ScanState::COMMENT);
          }
          break;
        case ScanState::COMMENT:
          if (lts.lookahead_is('#')) {
            consume_comment(lts);
          }
          else {
            lts.set_state(ScanState::NO_MATCH);
          }
          break;
        case ScanState::MATCH:
          tokens.push_back(lts.get_tok());
          debug(lts.get_tok()->info());
          lts.set_state_start();
          lts.matched();
          break;
        case ScanState::NO_MATCH:
          {
            stringstream err_ss;
            err_ss << "No matching token found for: " << lts.get_lookahead();
            lts.set_err(err_ss.str());
            lts.set_state_err();
          }
          break;
        case ScanState::DEDENT:
          for (int ded = 0; ded < lts.get_dedent_ct(); ded++) {
            Token* t = new Token(Tag::BLOCK_END, lts.get_line(), lts.get_col());
            tokens.push_back(t);
            debug(t->info());
          }
          lts.reset_dedent();
          lts.set_state_start();
          break;
        case ScanState::DONE:
          lts.stop_scanning();
          break;
        case ScanState::ERROR:
          lts.stop_scanning();
          break;
        default:
          lts.set_err("Lexer entered invalid state...");
          lts.set_state_err();
          lts.stop_scanning();
          break;
      }
    }

    delete buffer; // TODO: keep this in lts?

    if (lts.get_state() == ScanState::ERROR)
    {
      error("SYNTAX", lts.get_err(), lts.get_line(), lts.get_col());
      return -1;
    }

    close_dedents(tokens, lts);

    int token_size = tokens.size();
    stringstream result;
    result << "Lexer parsed " << token_size << " tokens";
    debug(result);
    log("Lexical analysis complete");
    return 0;
  }
}
