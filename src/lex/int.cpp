#include <sstream>
#include <string>
#include "int.hpp"
#include "tag.hpp"


namespace lex {
  using std::stringstream;

  std::string Int::info() {
    stringstream s;
    s << get_tag_name(tag) << ": " << val;
    return s.str();
  }
}
