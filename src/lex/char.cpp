#include <sstream>
#include <string>
#include "char.hpp"
#include "tag.hpp"


namespace lex {
  using std::stringstream;
  using std::string;

  string Char::info() {
    stringstream s;
    s << get_tag_name(tag) << ": " << val;
    return s.str();
  }
}
