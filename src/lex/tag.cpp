#include "tag.hpp"


namespace lex {

#define X(a, b) b,
  std::string get_tag_name(Tag tag) {
    const std::string tag_names[] {
      TAG_TABLE
    };

    return tag_names[tag];
  }
#undef X

}
