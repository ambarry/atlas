#include <string>
#include "tag.hpp"
#include "word.hpp"

namespace lex {
  using std::string;

  string Word::info(){
    return get_tag_name(tag) + ": " + lexeme;
  }
}
