#include <string>
#include "relop.hpp"

namespace lex {
  using std::string;
  
  string RelOp::info() {
    return get_tag_name(tag) + ": " + get_rel_op_name(op_type);
  }

#define X(a, b) b,
  string get_rel_op_name(RelOpType t) {
    const string rel_type_names[] {
      REL_OP_TABLE
    };

    return rel_type_names[t];
  }
#undef X

}
