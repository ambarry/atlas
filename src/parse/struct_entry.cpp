#include <pair>
#include <string>
#include <stringstream>
#include <unordered_map>

#include "struct_entry.hpp"

namespace parse {

  using std::string;
  using std::stringstream;
  using std::unordered_map;

  /*
   * Deletes all field entries on clean-up.
   */
  StructEntry::~StructEntry() {
    // clean field pointers
    for (auto it : fields) {
      delete it.second;
    }
  }

  /*
   * Checks if a struct has a field w/ this name.
   */
  bool StructEntry::has_field(string fd) {
    auto search = fields.find(fd);
    return search != fields.end();
  }

  /*
   * Returns the StructField of a field.
   */
  StructField* StructEntry::get_field(string fd) {
      return fields[fd];
  }

  /*
   * Attempts to add a field to the struct.
   * Checks if there is already a field by this name.
   * Returns a bool whether the operation succeeds.
   */
  bool StructEntry::add_field(string name, int type_code) {
    auto search = fields.find(name);
    if (search == fields.end()) {
      StructField* field = new StructField(type_code, name);
      fields[name] = type_code;
      return true;
    }
    else {
      return false; // ERROR: already defined
    }
  }

  /*
   * Returns information about this struct.
   */
  string StructEntry::info(){
    stringstream ss;
    ss << "STRUCT: " << name << std::endl;
    for (std::pair<string, StructField*> pr : fields){
      ss << "   field: " << pr.second->info() << std::endl;
    }
    return ss.str();
  }
}
