#include <iostream>
#include <stack>
#include <string>
#include <vector>

#include "non_terminals.hpp"
#include "parser.hpp"
#include "production.hpp"
#include "symbol_table.hpp"
#include "type_table.hpp"

namespace parse {

  using std::stack;
  using std::vector;
  using lex::Tag;
  using lex::Token;

  /**
   * The parser.
   * Parses a stream of tokens.
   * For now, using a vector of tokens, but
   * a concurrent queue might be nice...
   */
  int Parser::parse(vector<Token*>& tokens, SymbolTable& sym, TypeTable& tt) {
    log("Parsing beginning...");

    bool ok = true;
    stack<Production*> prods;

    // validate token stream
    if (tokens.empty()) {
      error("INPUT", "No tokens to parse", 0);
      ok = false;
    }

    // todo: clean as you, or at end, or what have you...

    if (ok) {
      tokens.push_back(new Token(Tag::END_SENTINAL, 0, 0));
      Token* tok = tokens.front();
      Production* focus = nullptr;

      // start parse at initial symbol
      prods.push(new EndSentinal());
      prods.push(new Goal());
      focus = prods.top();
      sym.initialize_scope(); // root scope

      while (ok) {
        // TODO: costly debug stack!!
        // TODO: unit tests!
        if (tok->is_a(Tag::END_SENTINAL) && focus->is_eof()) {
          log("Parse reached end successfully!");
          delete tok;
          break;
        }
        else if (focus->is_terminal() || focus->is_eof()) {
          // check if focus matches tok
          if (focus->is_match(*tok)){
            prods.pop();
            delete focus;
            // TODO: make this cleaner...
            // we also need to catch ints, struct decls, etc.
            // then get the id...            
            if (tok->is_a(Tag::BLOCK_START))
            {
              sym.initialize_scope();
            }
            else if (tok->is_a(Tag::BLOCK_END))
            {
              sym.finalize_scope();
            }
            delete tok; // TODO: consider not doing here?
            tokens.erase(tokens.begin());
            tok = tokens.front();
            focus = prods.top();
          }
          else {
            terminal_error("SYNTAX", "Could not find symbol at the top of stack:", tok->info(), focus->info(), tok->get_line());
            ok = false;
          }
        }
        else {
          // non-terminal!!! expand, making sure latest token can choose
          // a proper production
          prods.pop();
          debug("expanding focus: " + focus->info() + " with token: " + tok->info());
          ok = focus->expand(*tok, prods);
          if (!ok) {
            // error
            non_terminal_error("SYNTAX", "Could not expand production: ", tok->info(), focus->info(), tok->get_line());
          }
          else {
            delete focus;
            focus = prods.top();
          }
        }
      }
    }
    // TODO: do we finalize_scope on the last? or preserve exports?
    sym.finalize_scope();

    // clean up any if we errored early
    while (!prods.empty()) {
      delete(prods.top());
      prods.pop();
    }

    // TODO: in future:
    // alias table? (need to consider what a valid alias is, and if we should allow this...)

    log("Parsing complete.");
    return ok ? 0 : 1;
  }
}
