#include <iterator>
#include <string>
#include <pair>
#include <unordered_map>
#include "symbol_table.hpp"

namespace parse {
  using std::iterator;
  using std::pair;
  using std::string;
  using std::unordered_map;

  bool SymbolTable::insert(TableEntry* entry) {
    pair<iterator, bool> pr = current_scope->insert({entry->get_name(), entry});
    return pr.second;
  }

  TableEntry* SymbolTable::lookup(string name) {
    TableEntry* t = nullptr;
    SymbolTable* s = current_scope;
    while (t == nullptr && s != nullptr) {
      auto res = s->find(name);
      if (res != s->end()) {
        t = res->second;
      }
      else {
        s = current_scope->prev_scope;
      }
    }
    return t;
  }

  void SymbolTable::initialize_scope() {
    if (current_scope == nullptr) {
      current_scope = new SymbolTable();
    }
    else {
      current_scope =  new SymbolTable(current_scope);
    }
  }

  void SymbolTable::finalize_scope() {
    SymbolTable* to_delete = current_scope;
    current_scope = to_delete->prev_scope; // could be null if outermost
    delete to_delete;
  }

  SymbolTable::~SymbolTable() {
    // delete all entries in table
    for (pair<string, TableEntry*> pr : table) {
      delete pr.second;
    }
  }
}
