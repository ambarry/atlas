#include <stack>
#include "non_terminals.hpp"
#include "parser.hpp" // for log
#include "production.hpp"
#include "tag.hpp"
#include "terminals.hpp"
#include "token.hpp"

namespace parse {

  using std::stack;
  using lex::Tag;
  using lex::Token;

  // -----------------------------------------------
  // --- Non Terminals ---
  // -----------------------------------------------

  bool Goal::expand(Token &tok, Prods& prods) {
    //prods.push(new B_Expr());
    prods.push(new Expr_List());
    return true;
  }

  // -----------------------------------------------
  // --- Definitions ---
  // -----------------------------------------------

  bool Definition::expand(Token &tok, Prods& prods) {
    if (tok.is_a(Tag::STRUCT)){
      prods.push(new Struct_Definition());
      prods.push(new Assign_Op());
      prods.push(new Struct());
    }
    else if (tok.is_a(Tag::UNION)) {
      prods.push(new Union_Definition());
      prods.push(new Assign_Op());
      prods.push(new Union());
    }
    else {
      // must be type, if that fails, let it error
      prods.push(new B_Expr());
      prods.push(new Assign_Op());
      prods.push(new Type());
    }
    return true;
  }

  bool Index::expand(Token& tok, Prods& prods) {
    prods.push(new R_Bracket());
    prods.push(new Term_Expr());
    prods.push(new L_Bracket());
    return true;
  }

  bool Id_List::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::ID)) {
      prods.push(new Id_List_Prime());
      prods.push(new Id());
      return true;
    }
    else if (tok.is_a(Tag::WILDCARD)){
      prods.push(new Id_List_Prime());
      prods.push(new Wildcard());
      return true;
    }
    else {
      return false;
    }
  }

  bool Id_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::COMMA)) {
      prods.push(new Id_List());
      prods.push(new Comma());
    }
    // allow e
    return true;
  }

  bool Struct_Definition::expand(Token& tok, Prods& prods) {
    prods.push(new Block_End());
    prods.push(new Field_Def_List());
    prods.push(new Block_Start());
    return true;
  }

  bool Field_Def_List::expand(Token& tok, Prods& prods) {
    prods.push(new Field_Def_List_Prime());
    prods.push(new Type());
    prods.push(new Colon());
    prods.push(new Id());
    prods.push(new Amp());
    return true;
  }

  bool Field_Def_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::AMP)) {
      prods.push(new Field_Def_List());
    }
    // allow e
    return true;
  }

  bool Union_Definition::expand(Token& tok, Prods& prods) {
    prods.push(new Block_End());
    prods.push(new Union_Case_List());
    prods.push(new Block_Start());
    return true;
  }

  bool Union_Case_List::expand(Token& tok, Prods& prods) {
    prods.push(new Union_Case_List_Prime());
    prods.push(new Opt_Union_Type());
    prods.push(new Id());
    prods.push(new Pipe());
    return true;
  }

  bool Union_Case_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::PIPE)) {
      prods.push(new Union_Case_List());
    }
    // allow e
    return true;
  }

  bool Opt_Union_Type::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::COLON)) {
      prods.push(new Type());
      prods.push(new Colon());
    }
    // allow e
    return true;
  }

  // -----------------------------------------------
  // --- Types ---
  // -----------------------------------------------

  bool Type::expand(Token& tok, Prods& prods) {
    prods.push(new Type_Postfix());
    prods.push(new Type_Prime());
    return true;
  }

  bool Type_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::L_PAREN)) {
      prods.push(new R_Paren());
      prods.push(new Type());
      prods.push(new L_Paren());
    }
    else if (tok.is_a(Tag::PTR_OP)) {
      prods.push(new Type_Prime());
      prods.push(new Ptr_Op());
    }
    else if (tok.is_a(Tag::DOLLAR)) {
      prods.push(new List_Type());
    }
    else if (tok.is_a(Tag::L_BRACKET)) {
      prods.push(new Array_Type());
    }
    else {
      // must be standard type
      prods.push(new Standard_Type());
    }
    return true;
  }

  bool Type_List::expand(Token& tok, Prods& prods) {
    prods.push(new Type_List_Prime());
    prods.push(new Type_Prime());
    return true;
  }

  bool Type_List_Prime::expand(Token& tok, Prods& prods) {
    if (is_type_token(tok)) {
      prods.push(new Type_List());
    }
    // allow e
    return true;
  }

  bool Type_Postfix::expand(Token& tok, Prods& prods) {
    // could be type_list or e
    // to validate type_list, check if it could be a type
    // TODO: clean this up!
    if (is_type_token(tok)) {
      prods.push(new Type_Prime());
      prods.push(new To_Op());
      prods.push(new Type_List());
    }
    else if (tok.is_a(Tag::TO_OP)) {
      prods.push(new Type_Prime());
      prods.push(new To_Op());
    }
    // allow e
    return true;
  }

  bool Standard_Type::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::ID)) {
      prods.push(new Opt_Generic_Suffix());
      prods.push(new Id());
    }
    else if (tok.is_a(Tag::L_BRACE)) {
      prods.push(new R_Brace());
      prods.push(new Tuple_Type_List());
      prods.push(new L_Brace());
    }
    else {
      // must be built-in-type
      prods.push(new Built_In_Type());
    }
    return true;
  }

  bool Built_In_Type::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::CHAR_T)){
      prods.push(new Char_T());
      return true;
    }
    else if (tok.is_a(Tag::STRING_T)) {
      prods.push(new String_T());
      return true;
    }
    else if (tok.is_a(Tag::INT_T)) {
      prods.push(new Int_T());
      return true;
    }
    else if (tok.is_a(Tag::BOOL_T)) {
      prods.push(new Bool_T());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected type", tok.info(),
        info(), tok.get_line());
      return false;
    }
  }

  bool Generic_Suffix::expand(Token& tok, Prods& prods) {
    prods.push(new R_Caret());
    prods.push(new Type());
    prods.push(new L_Caret());
    return true;
  }

  bool Opt_Generic_Suffix::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::L_CARET)) {
      prods.push(new Generic_Suffix());
    }
    // allow e
    return true;
  }

  bool Tuple_Type_List::expand(Token& tok, Prods& prods) {
    prods.push(new Tuple_Type_List_Prime());
    prods.push(new Type());
    return true;
  }

  bool Tuple_Type_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::COMMA)) {
      prods.push(new Tuple_Type_List());
      prods.push(new Comma());
    }
    // allow e
    return true;
  }

  bool List_Type::expand(Token& tok, Prods& prods) {
    prods.push(new R_Bracket());
    prods.push(new Type());
    prods.push(new L_Bracket());
    prods.push(new Dollar());
    return true;
  }

  bool Array_Type::expand(Token& tok, Prods& prods) {
    prods.push(new R_Bracket());
    prods.push(new Expr());
    prods.push(new Colon());
    prods.push(new Type());
    prods.push(new L_Bracket());
    return true;
  }

  // -----------------------------------------------
  // --- Primary Expressions ---
  // -----------------------------------------------

  bool B_Expr::expand(Token &tok, Prods& prods) {
    prods.push(new Block_End());
    prods.push(new Expr_List());
    prods.push(new Block_Start());
    return true;
  }

  bool Expr_List::expand(Token &tok, Prods& prods) {
    prods.push(new Expr_List_Prime());
    prods.push(new Expr());
    return true;
  }

  bool Expr_List_Prime::expand(Token& tok, Prods& prods) {
    // check expr... ugh...
    if (is_expr_token(tok)) {
      prods.push(new Expr_List());
    }
    // allow e
    return true;
  }

  bool Expr::expand(Token& tok, Prods& prods) {
    prods.push(new Forward_Pipe_Expr());
    prods.push(new Expr_Prime());
    return true;
  }

  bool Expr_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::LAMBDA)){
      prods.push(new Lambda_Expr());
    }
    else if (tok.is_a(Tag::MATCH)) {
      prods.push(new Match_Expr());
    }
    else {
      // must be term_expr
      prods.push(new Term_Expr());
    }
    return true;
  }

  bool Forward_Pipe_Expr::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::FORWARD_PIPE_OP)) {
      prods.push(new Expr());
      prods.push(new Forward_Pipe_Op());
    }
    // allow e
    return true;
  }

  bool Term_Expr::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::NOT)) {
      prods.push(new Term_Prefix_Expr());
    }
    else {
      prods.push(new Term_Postfix_Expr());
    }
    return true;
  }

  bool Term_Postfix_Expr::expand(Token& tok, Prods& prods) {
    prods.push(new Term_Postfix());
    prods.push(new Term());
    return true;
  }

  bool Term_Postfix::expand(Token& tok, Prods& prods) {
    if (is_term_token(tok) || tok.is_a(Tag::ARG_CONT_OP)) {
      prods.push(new Argument_List());
    }
    else if (tok.is_a(Tag::AND) || tok.is_a(Tag::OR)) {
      prods.push(new Bool_Postfix());
    }
    else if (tok.is_a(Tag::L_CARET)
      || tok.is_a(Tag::R_CARET)
      || tok.is_a(Tag::REL_OP)) {
      prods.push(new Rel_Postfix());
    }
    else if (is_arithmetic_op_token(tok)) {
      prods.push(new Arithmetic_Postfix());
    }
    else if (tok.is_a(Tag::ASSIGN_OP)) {
      prods.push(new B_Expr());
      prods.push(new Assign_Op());
    }
    else if (tok.is_a(Tag::COLON)){
      // TODO: anyway to check what side we're on?
      // TODO: must thread the identifier throughout, so we can assign properly
      // to the symbol table once a type is established, or otherwise error
      // if a redefinition
      // although, could also be tuple unpack def, which is really 2 defs...
      // NOTE: a keyword would be extremely useful here, as we'd know to commit
      // a symbol...
      // really, we just need the term, which should be in prods
      // but we should have pushed the id onto the semantic stack...
      prods.push(new Definition());
      prods.push(new Colon());
    }
    // allow e
    return true;
  }

  bool Argument_List::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::ARG_CONT_OP)) {
      prods.push(new Argument_List_Prime());
      prods.push(new Term());
      prods.push(new Arg_Cont_Op());
    }
    else {
      prods.push(new Argument_List_Prime());
      prods.push(new Term());
    }
    return true;
  }

  bool Argument_List_Prime::expand(Token& tok, Prods& prods) {
    if (is_term_token(tok) || tok.is_a(Tag::ARG_CONT_OP)){
      prods.push(new Argument_List());
    }
    // allow e
    return true;
  }

  bool Term_Prefix_Expr::expand(Token& tok, Prods& prods) {
    prods.push(new Bool_Prefix_Expr());
    return true;
  }

  bool Term::expand(Token& tok, Prods& prods) {
    prods.push(new Dot_Expr());
    prods.push(new Term_Prime());
    return true;
  }

  bool Term_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::PTR_OP)) {
      prods.push(new Ptr_Expr());
    }
    else if (tok.is_a(Tag::L_BRACE)) {
      prods.push(new R_Brace());
      prods.push(new B_Term_Expr_List());
      prods.push(new L_Brace());
    }
    else if (tok.is_a(Tag::L_BRACKET)) {
      prods.push(new R_Bracket());
      prods.push(new B_Term_Expr_List());
      prods.push(new L_Bracket());
    }
    else if (tok.is_a(Tag::L_PAREN)) {
      prods.push(new R_Paren());
      prods.push(new Expr());
      prods.push(new L_Paren());
    }
    else if (tok.is_a(Tag::DOLLAR)) {
      prods.push(new Dollar_Term());
      prods.push(new Dollar());
    }
    else {
      // must be a factor (could check here too...)
      prods.push(new Factor());
    }
    return true;
  }

  bool Dollar_Term::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::L_BRACE)) {
      prods.push(new R_Brace());
      prods.push(new B_Assignment_List());
      prods.push(new L_Brace());
      return true;
    }
    else if (tok.is_a(Tag::L_BRACKET)) {
      prods.push(new R_Bracket());
      prods.push(new B_Term_Expr_List());
      prods.push(new L_Bracket());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Illegal expression following $: ",
        tok.info(), info(), tok.get_line());
      return false;
    }
  }

  bool Dot_Expr::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::DOT_OP)) {
      prods.push(new Dot_Op_Postfix());
      prods.push(new Dot_Op());
    }
    // allow e
    return true;
  }

  bool Dot_Op_Postfix::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::ID)) {
      prods.push(new Dot_Expr());
      prods.push(new Id());
      return true;
    }
    else if (tok.is_a(Tag::L_BRACKET)) {
      prods.push(new Dot_Expr());
      prods.push(new Index());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Illegal expression following .: ",
        tok.info(), info(), tok.get_line());
      return false;
    }
  }

  bool Factor::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::CHAR)) {
      prods.push(new Char());
      return true;
    }
    else if (tok.is_a(Tag::STRING)) {
      prods.push(new String());
      return true;
    }
    else if (tok.is_a(Tag::INT)) {
      prods.push(new Int());
      return true;
    }
    else if (tok.is_a(Tag::_TRUE) || tok.is_a(Tag::_FALSE)) {
      prods.push(new Bool());
      return true;
    }
    else if (tok.is_a(Tag::ID)) {
      prods.push(new Id());
      return true;
    }
    else if (tok.is_a(Tag::WILDCARD)) {
      prods.push(new Wildcard());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Invalid factor: ", tok.info(), info(),
        tok.get_line());
      return false;
    }
  }

  bool B_Term_Expr_List::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::BLOCK_START)) {
      prods.push(new Block_End());
      prods.push(new Term_Expr_List());
      prods.push(new Block_Start());
    }
    else {
      // can cheat a little here: if not block, default to term_expr_list
      prods.push(new Term_Expr_List());
    }
    return true;
  }

  bool Term_Expr_List::expand(Token& tok, Prods& prods) {
    // can cheat a little here too: if we get a closing brace, bracket, or
    // block_end we know it's empty, e.g. e, and can return true.
    // otherwise, let the normal expr expansion determine validity
    if (!(
           tok.is_a(Tag::R_BRACKET)
           || tok.is_a(Tag::R_BRACE)
           || tok.is_a(Tag::BLOCK_END)
         )
       ) {
         prods.push(new Term_Expr_List_Prime());
         prods.push(new Expr());
    }
    // allow e
    return true;
  }

  bool Term_Expr_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::COMMA)) {
      prods.push(new Term_Expr_List());
      prods.push(new Comma());
    }
    // allow e
    return true;
  }

  bool B_Assignment_List::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::BLOCK_START)) {
      prods.push(new Block_End());
      prods.push(new Assignment_List());
      prods.push(new Block_Start());
    }
    else {
      // let Assignment_List handle
      prods.push(new Assignment_List());
    }
    return true;
  }

  bool Assignment_List::expand(Token& tok, Prods& prods) {
    prods.push(new Assignment_List_Prime());
    prods.push(new Expr());
    //prods.push(new Assign_Op());
    prods.push(new Colon());
    prods.push(new Id());
    return true;
  }

  bool Assignment_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::COMMA)) {
      prods.push(new Assignment_List());
      prods.push(new Comma());
    }
    // allow e
    return true;
  }

  // -----------------------------------------------
  // --- Lambda Expressions ---
  // -----------------------------------------------

  bool Lambda_Expr::expand(Token& tok, Prods& prods) {
    prods.push(new B_Expr());
    prods.push(new Arrow_Op());
    prods.push(new Binding_List());
    prods.push(new Lambda());
    return true;
  }

  bool Binding_List::expand(Token& tok, Prods& prods) {
    prods.push(new Binding_List_Prime());
    prods.push(new Id());
    return true;
  }

  bool Binding_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::ID)) {
      prods.push(new Binding_List());
    }
    // allow e
    return true;
  }

  // -----------------------------------------------
  // --- Pointer Expressions ---
  // -----------------------------------------------

  bool Ptr_Expr::expand(Token& tok, Prods& prods) {
    prods.push(new Expr());
    prods.push(new Ptr_Op());
    return true;
  }

  // -----------------------------------------------
  // --- Boolean Expressions ---
  // -----------------------------------------------

  bool Bool::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::_TRUE)) {
      prods.push(new True());
      return true;
    }
    else if (tok.is_a(Tag::_FALSE)) {
      prods.push(new False());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected bool",
        tok.info(), info(), tok.get_line());
      return false;
    }
  }

  bool Bool_Postfix::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::AND)) {
      prods.push(new Expr());
      prods.push(new And());
    }
    else if (tok.is_a(Tag::OR)){
      prods.push(new Expr());
      prods.push(new Or());
    }
    else if (is_relop_token(tok)){
      prods.push(new Expr());
      prods.push(new Rel());
    }
    else {
      non_terminal_error("SYNTAX", "Expected boolean or relative operator",
        tok.info(), info(), tok.get_line());
      return false;
    }
    return true;
  }

  bool Rel_Postfix::expand(Token& tok, Prods& prods) {
    prods.push(new Expr());
    prods.push(new Rel());
    return true;
  }

  bool Rel::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::L_CARET)) {
      prods.push(new L_Caret()); // NOTE: must push what will match terminal token
      return true;
    }
    else if (tok.is_a(Tag::R_CARET)) {
      prods.push(new R_Caret());
      return true;
    }
    else if (tok.is_a(Tag::REL_OP)) {
      prods.push(new Rel_Op());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Invalid relative operator",
        tok.info(), info(), tok.get_line());
      return false;
    }
  }

  bool Bool_Prefix_Expr::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::NOT)) {
      prods.push(new Expr());
      prods.push(new Not());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected 'not'", tok.info(), info(),
        tok.get_line());
      return false;
    }
  }

  // -----------------------------------------------
  // --- Arithmetic Expressions ---
  // -----------------------------------------------

  bool Arithmetic_Postfix::expand(Token& tok, Prods& prods) {
    prods.push(new Expr());
    prods.push(new Arithmetic_Op());
    return true;
  }

  bool Arithmetic_Op::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::ADD_OP)) {
      prods.push(new Add_Op());
      return true;
    }
    else if (tok.is_a(Tag::SUBTRACT_OP)) {
      prods.push(new Subtract_Op());
      return true;
    }
    else if (tok.is_a(Tag::MULT_OP)) {
      prods.push(new Mult_Op());
      return true;
    }
    else if (tok.is_a(Tag::DIVIDE_OP)) {
      prods.push(new Divide_Op());
      return true;
    }
    else if (tok.is_a(Tag::MODULO_OP)) {
      prods.push(new Modulo_Op());
      return true;
    }
    else if (tok.is_a(Tag::EXP_OP)) {
      prods.push(new Exp_Op());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected arithmetic op", tok.info(),
        info(), tok.get_line());
      return false;
    }
  }

  // -----------------------------------------------
  // --- Pattern Matching Expressions ---
  // -----------------------------------------------

  bool Match_Expr::expand(Token& tok, Prods& prods) {
    prods.push(new Block_End());
    prods.push(new Rule_List());
    prods.push(new Block_Start());
    prods.push(new With());
    prods.push(new Expr());
    prods.push(new Match());
    return true;
  }

  bool Rule_List::expand(Token& tok, Prods& prods) {
    prods.push(new Rule_List_Prime());
    prods.push(new Rule());
    return true;
  }

  bool Rule_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::PIPE)) {
      prods.push(new Rule_List());
    }
    // allow e
    return true;
  }

  bool Rule::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::PIPE)) {
      prods.push(new B_Expr());
      prods.push(new Arrow_Op());
      prods.push(new Pattern_Guard());
      prods.push(new Pattern());
      prods.push(new Pipe());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected '|' in match case", tok.info(), info(),
        tok.get_line());
      return false;
    }
  }

  bool Pattern::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::L_BRACKET) || tok.is_a(Tag::DOLLAR)) {
      prods.push(new Collection_Pattern());
    }
    else if (tok.is_a(Tag::L_BRACE)) {
      prods.push(new Tuple_Pattern());
    }
    else {
      // assume factor
      prods.push(new Pattern_Factor_Postfix());
      prods.push(new Factor());
    }
    return true;
  }

  bool Pattern_Guard::expand(Token& tok, Prods &prods) {
    if (tok.is_a(Tag::WHEN)) {
      prods.push(new Expr());
      prods.push(new When());
    }
    // allow e
    return true;
  }

  bool Pattern_Factor_Postfix::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::COLON)) {
      // cons pattern
      prods.push(new Pattern());
      prods.push(new Colon()); // TODO: this should probably be its own operator in the lexer
      prods.push(new Colon());

    }
    else if (tok.is_a(Tag::PIPE)) {
      prods.push(new Pattern());
      prods.push(new Pipe());
    }
    else if (tok.is_a(Tag::AMP)){
      prods.push(new Pattern());
      prods.push(new Amp());
    }
    else if (is_factor_token(tok)) {
      prods.push(new Factor());
    }
    // allow e
    return true;
  }

  bool Collection_Pattern::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::L_BRACKET)) {
      prods.push(new R_Bracket());
      prods.push(new Pattern_List());
      prods.push(new L_Bracket());
      return true;
    }
    else if (tok.is_a(Tag::DOLLAR)) {
      prods.push(new R_Bracket());
      prods.push(new Pattern_List());
      prods.push(new L_Bracket());
      prods.push(new Dollar());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected collection pattern", tok.info(), info(),
        tok.get_line());
      return false;
    }
  }

  bool Tuple_Pattern::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::L_BRACE)) {
      prods.push(new R_Brace());
      prods.push(new Pattern_List());
      prods.push(new L_Brace());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected tuple pattern", tok.info(), info(),
        tok.get_line());
      return false;
    }
  }

  bool Pattern_List::expand(Token& tok, Prods& prods) {
    // TODO: parens?
    if (is_factor_token(tok)
          || tok.is_a(Tag::DOLLAR)
          || tok.is_a(Tag::L_BRACKET)
          || tok.is_a(Tag::L_BRACE)) {
      prods.push(new Pattern_List_Prime());
      prods.push(new Pattern());
    }
    // allow e
    return true;
  }

  bool Pattern_List_Prime::expand(Token& tok, Prods& prods) {
    if (tok.is_a(Tag::COMMA)) {
      prods.push(new Pattern_List());
      prods.push(new Comma());
      return true;
    }
    else {
      non_terminal_error("SYNTAX", "Expected ','", tok.info(), info(), tok.get_line());
      return false;
    }
  }

  // -----------------------------------------------
  // --- Helpers ---
  // -----------------------------------------------

  bool is_type_token(Token& tok) {
    return
      tok.is_a(Tag::L_PAREN) ||
      tok.is_a(Tag::PTR_OP) ||
      tok.is_a(Tag::DOLLAR) ||
      tok.is_a(Tag::L_BRACKET) ||

      // standard type
      tok.is_a(Tag::L_BRACE) ||
      tok.is_a(Tag::ID) ||

      // built-in (under standard)
      tok.is_a(Tag::CHAR_T) ||
      tok.is_a(Tag::STRING_T) ||
      tok.is_a(Tag::INT_T) ||
      tok.is_a(Tag::BOOL_T);
  }

  bool is_expr_token(Token& tok) {
    return
      tok.is_a(Tag::MATCH) // match expr
      || tok.is_a(Tag::LAMBDA) // lambda expr
      || tok.is_a(Tag::NOT) // term prefix / bool prefix
      // term postfix, aka term -> term'
      || is_term_token(tok);

  }

  bool is_term_token(Token& tok) {
    return
      is_factor_token(tok)
      || tok.is_a(Tag::PTR_OP)
      || tok.is_a(Tag::L_BRACE)
      || tok.is_a(Tag::L_BRACKET)
      || tok.is_a(Tag::L_PAREN)
      || tok.is_a(Tag::DOLLAR);
  }

  bool is_arithmetic_op_token(Token& tok){
    return
      tok.is_a(Tag::ADD_OP)
      || tok.is_a(Tag::SUBTRACT_OP)
      || tok.is_a(Tag::MULT_OP)
      || tok.is_a(Tag::DIVIDE_OP)
      || tok.is_a(Tag::MODULO_OP)
      || tok.is_a(Tag::EXP_OP);
  }

  bool is_relop_token(Token& tok) {
    return
      tok.is_a(Tag::REL_OP)
      || tok.is_a(Tag::L_CARET)
      || tok.is_a(Tag::R_CARET);
  }

  bool is_factor_token(Token& tok) {
    return
      tok.is_a(Tag::CHAR)
      || tok.is_a(Tag::STRING)
      || tok.is_a(Tag::INT)
      || tok.is_a(Tag::BOOL) // NOTE: this tag probably never used...
      || tok.is_a(Tag::ID)
      || tok.is_a(Tag::WILDCARD)
      || tok.is_a(Tag::_TRUE)
      || tok.is_a(Tag::_FALSE);
  }
}
