#include "production.hpp"
#include "tag.hpp"
#include "token.hpp"
#include "terminals.hpp"

namespace parse {

  using lex::Tag;
  using lex::Token;

  bool Add_Op::is_match(Token &tok) {
    return tok.is_a(Tag::ADD_OP);
  }

  bool Amp::is_match(Token& tok) {
    return tok.is_a(Tag::AMP);
  }

  bool And::is_match(Token &tok) {
    return tok.is_a(Tag::AND);
  }

  bool Arg_Cont_Op::is_match(Token &tok) {
    return tok.is_a(Tag::ARG_CONT_OP);
  }

  bool Arrow_Op::is_match(Token &tok) {
    return tok.is_a(Tag::ARROW_OP);
  }

  bool Assign_Op::is_match(Token &tok) {
    return tok.is_a(Tag::ASSIGN_OP);
  }

  bool Block_Start::is_match(Token& tok) {
    return tok.is_a(Tag::BLOCK_START);
  }

  bool Block_End::is_match(Token& tok) {
    return tok.is_a(Tag::BLOCK_END);
  }

  bool Bool_T::is_match(Token &tok) {
    return tok.is_a(Tag::BOOL_T);
  }

  bool Char::is_match(Token &tok) {
    return tok.is_a(Tag::CHAR);
  }

  bool Char_T::is_match(Token &tok) {
    return tok.is_a(Tag::CHAR_T);
  }

  bool Colon::is_match(Token &tok) {
    return tok.is_a(Tag::COLON);
  }

  bool Comma::is_match(Token &tok) {
    return tok.is_a(Tag::COMMA);
  }

  bool Divide_Op::is_match(Token &tok) {
    return tok.is_a(Tag::DIVIDE_OP);
  }

  bool Dollar::is_match(Token& tok) {
    return tok.is_a(Tag::DOLLAR);
  }

  bool Dot_Op::is_match(Token& tok) {
    return tok.is_a(Tag::DOT_OP);
  }

  bool Exp_Op::is_match(Token& tok) {
    return tok.is_a(Tag::EXP_OP);
  }

  bool False::is_match(Token& tok) {
    return tok.is_a(Tag::_FALSE);
  }

  bool Forward_Pipe_Op::is_match(Token& tok) {
    return tok.is_a(Tag::FORWARD_PIPE_OP);
  }

  bool Id::is_match(Token& tok) {
    return tok.is_a(Tag::ID);
  }

  bool Int::is_match(Token& tok) {
    return tok.is_a(Tag::INT);
  }

  bool Int_T::is_match(Token& tok) {
    return tok.is_a(Tag::INT_T);
  }

  bool L_Brace::is_match(Token& tok) {
    return tok.is_a(Tag::L_BRACE);
  }

  bool L_Bracket::is_match(Token& tok) {
    return tok.is_a(Tag::L_BRACKET);
  }

  bool L_Caret::is_match(Token& tok) {
    return tok.is_a(Tag::L_CARET);
  }

  bool L_Paren::is_match(Token& tok) {
    return tok.is_a(Tag::L_PAREN);
  }

  bool Lambda::is_match(Token& tok) {
    return tok.is_a(Tag::LAMBDA);
  }

  bool Match::is_match(Token& tok) {
    return tok.is_a(Tag::MATCH);
  }

  bool Modulo_Op::is_match(Token& tok) {
    return tok.is_a(Tag::MODULO_OP);
  }

  bool Mult_Op::is_match(Token& tok) {
    return tok.is_a(Tag::MULT_OP);
  }

  bool Not::is_match(Token& tok) {
    return tok.is_a(Tag::NOT);
  }

  bool Or::is_match(Token& tok) {
    return tok.is_a(Tag::OR);
  }

  bool Ptr_Op::is_match(Token& tok) {
    return tok.is_a(Tag::PTR_OP);
  }

  bool Pipe::is_match(Token& tok) {
    return tok.is_a(Tag::PIPE);
  }

  bool R_Brace::is_match(Token& tok) {
    return tok.is_a(Tag::R_BRACE);
  }

  bool R_Bracket::is_match(Token& tok) {
    return tok.is_a(Tag::R_BRACKET);
  }

  bool R_Caret::is_match(Token& tok) {
    return tok.is_a(Tag::R_CARET);
  }

  bool R_Paren::is_match(Token& tok) {
    return tok.is_a(Tag::R_PAREN);
  }

  bool Rel_Op::is_match(Token& tok) {
    return tok.is_a(Tag::REL_OP);
  }

  bool String::is_match(Token& tok) {
    return tok.is_a(Tag::STRING);
  }

  bool String_T::is_match(Token& tok) {
    return tok.is_a(Tag::STRING_T);
  }

  bool Struct::is_match(Token& tok){
    return tok.is_a(Tag::STRUCT);
  }

  bool Subtract_Op::is_match(Token& tok) {
    return tok.is_a(Tag::SUBTRACT_OP);
  }

  bool To_Op::is_match(Token& tok) {
    return tok.is_a(Tag::TO_OP);
  }

  bool True::is_match(Token& tok) {
    return tok.is_a(Tag::_TRUE);
  }

  bool Union::is_match(Token& tok) {
    return tok.is_a(Tag::UNION);
  }

  bool When::is_match(Token& tok) {
    return tok.is_a(Tag::WHEN);
  }

  bool With::is_match(Token& tok) {
    return tok.is_a(Tag::WITH);
  }

  bool Wildcard::is_match(Token& tok) {
    return tok.is_a(Tag::WILDCARD);
  }
}
