#include "type_table.hpp"

/*
 * Initializes TypeTable with built-ins.
 */
TypeTable::TypeTable() {
  // NOTE: TypeInfo shouldn't be needed for built-ins
  TypeEntry* bool_type = new TypeEntry(BOOL, "bool", TypeCat.BUILT_IN, null);
  TypeEntry* char_type = new TypeEntry(CHAR, "char", TypeCat.BUILT_IN, null);
  TypeEntry* int_type = new TypeEntry(INT, "int" TypeCat.BUILT_IN, null);
  TypeEntry* string_type = new TypeEntry(STRING, "string", TypeCat.BUILT_IN, null);
  types.push_back(bool_type);
  types.push_back(char_type);
  types.push_back(int_type);
  types.push_back(string_type);
  // TODO: init list, array as generics?
}

/*
 * Deletes all types in the TypeTable on clean-up.
 */
TypeTable::~TypeTable() {
  // Clean all entries
  for (TypeEntry* tep : types) {
    delete tep;
  }
}

/*
 * Adds a new type to the TypeTable.
 * It is up to the master symbol table to check for existence.
 */
int TypeTable::add_type(TypeEntry* te) {
  types.push_back(te);
  return apply_next(); // index
}
