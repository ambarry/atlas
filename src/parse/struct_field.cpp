#include <string>
#include <stringstream>

#include "struct_field.hpp"

namespace parse {
  using std::string;

  string StructField::info() {
    stringstream ss;
    ss << "Name: " << name << ", Code: " << type_code << std::endl;
    return ss.str();
  }
}
