#include <fstream>
#include <sstream>
#include <string>

#include <vector>

#include "lexer.hpp"
#include "logger.hpp"
#include "parser.hpp"
#include "token.hpp"

using logger::log;
using logger::error;
using lex::Lexer;
using lex::Token;
using parse::Parser;
using std::vector;

bool ok(int res);

int main(int argc, char const *argv[]) {

  // TODO: except string or filename? any way to stream better?
  // TODO: error handle on the IO
  // TODO: ultimately, should look for files / sln-esque yml file in dir
  log("main", "starting atlas...");
  if (argc < 2){
    error("usage", "lexer <file>"); //used to be input
    return -1;
  }

  // lex the input
  Lexer lexer;
  vector<Token*> tokens;
  std::ifstream input;
  input.open(argv[1]);
  log("main", "starting lexer...");
  int res = lexer.scan(input, tokens);
  if (ok(res)) {
    log("main", "...finished lexing!");
  }
  else {
    log("main", "...error lexing.");
  }
  input.close();
  log("main", "input closed.");

  // parse the tokens
  // (TODO: this should be concurrent / stream / concurrent queue!)
  SymbolTable sym;
  TypeTable tt;
  if (ok(res)) {
    Parser parser;
    log("main", "starting parser...");
    res = parser.parse(tokens, root_syms, types);
  }

  if (ok(res)) {
    log("main", "...finished parsing!");
  }
  else {
    log("main", "...error parsing.");
  }

  // clean tokens if any remain
  for (size_t i = 0; i < tokens.size() -1; i++) {
    if (tokens[i] != nullptr) {
      delete tokens[i];
    }
  }
  log("main", "cleaned any remaining tokens");

  return res;
}


bool ok(int res) {
  return res == 0;
}
